package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.model.Category;
import planner.model.Task;
import planner.util.UserInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CategoryDaoImpl is the class implements PlannerDao interface for Category model class.
 */
public class CategoryDaoImpl implements PlannerDao<Category> {

    /**
     * A variable representing category id column name in DB.
     */
    private final static String CATEGORY_ID_COLUMN_NAME = "ID";

    /**
     * A variable representing category name column name in DB.
     */
    private final static String CATEGORY_NAME_COLUMN_NAME = "NAME";

    /**
     * A variable representing category description column name in DB.
     */
    private final static String CATEGORY_DESCRIPTION_COLUMN_NAME = "DESCRIPTION";

    /**
     * A variable representing the only instance of this class.
     */
    private static CategoryDaoImpl instance;

    static {
        instance = new CategoryDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of CategoryDaoImpl class
     */
    public static CategoryDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in the table "category" in DB.
     *
     * @param category the object which will be added in database
     */
    public void add(Category category) {
        Connection connection = DBConnection.getConnection();

        PreparedStatement addQuery = null;
        try {
            addQuery = connection.prepareStatement("INSERT INTO category (name, description, owner) VALUES (?, ?, ?);");
            addQuery.setString(1, category.getName());
            addQuery.setString(2, category.getDescription());
            addQuery.setString(3, UserInfo.getUserLogin());
            addQuery.executeUpdate();
            addQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "category" in DB.
     *
     * @param category the object which will be deleted from database
     */
    public void delete(Category category) {
        Connection connection = DBConnection.getConnection();

        try {
            PreparedStatement deleteQuery = connection.prepareStatement("DELETE FROM category WHERE ID=?;");
            deleteQuery.setInt(1, category.getId());
            deleteQuery.executeUpdate();
            deleteQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting some record from the table "category" in DB.
     *
     * @param key the primary key of record which will be gotten
     * @return the object of the Category class with set primary key
     */
    public Category getById(int key) throws SQLException {
        Connection connection = DBConnection.getConnection();

        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM CATEGORY where id=? and owner=?");
        getQuery.setInt(1, key);
        getQuery.setString(2, UserInfo.getUserLogin());
        ResultSet categoryResult = getQuery.executeQuery();

        Category category = new Category();
        while (categoryResult.next()) {
            category.setId(key);
            category.setName(categoryResult.getString(CATEGORY_NAME_COLUMN_NAME));
            category.setDescription(categoryResult.getString(CATEGORY_DESCRIPTION_COLUMN_NAME));
        }

        categoryResult.close();
        getQuery.close();

        return category;
    }

    /**
     * The method used for getting all records from the table "category" in DB. Has no input parameters
     *
     * @return List of all Category objects.
     */
    public List<Category> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();

        PreparedStatement getAllQuery = connection.prepareStatement("SELECT * FROM CATEGORY WHERE owner=? order by id;");
        getAllQuery.setString(1, UserInfo.getUserLogin());
        ResultSet categoryResult = getAllQuery.executeQuery();

        List<Category> categories = new ArrayList<Category>();

        while (categoryResult.next()) {
            Category category = new Category();
            category.setId(categoryResult.getInt(CATEGORY_ID_COLUMN_NAME));
            category.setName(categoryResult.getString(CATEGORY_NAME_COLUMN_NAME));
            category.setDescription(categoryResult.getString(CATEGORY_DESCRIPTION_COLUMN_NAME));
            categories.add(category);
        }
        categoryResult.close();
        getAllQuery.close();

        return categories;
    }

    /**
     * The method used for update some record in the table "category" in DB.
     *
     * @param category the object which will be updated in database
     */
    public void update(Category category) {
        Connection connection = DBConnection.getConnection();

        try {
            PreparedStatement updateQuery = connection.prepareStatement("UPDATE category  SET  name = ?, description =?, owner = ?  WHERE id = ?;");
            updateQuery.setString(1, category.getName());
            updateQuery.setString(2, category.getDescription());
            updateQuery.setString(3, UserInfo.getUserLogin());
            updateQuery.setInt(4, category.getId());
            updateQuery.executeUpdate();
            updateQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting all records from the table "category" in DB with particular task. Has no input parameters
     *
     * @return List of all Category objects with set task.
     */
    public Category getByTask(Task task) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM category INNER JOIN task ON task.category = category.id " +
                " WHERE (task.owner =?) AND (task.id=?) ");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, task.getId());
        ResultSet categoryResult = getQuery.executeQuery();
        Category category = new Category();
        while (categoryResult.next()) {
            category.setId(categoryResult.getInt(CATEGORY_ID_COLUMN_NAME));
            category.setName(categoryResult.getString(CATEGORY_NAME_COLUMN_NAME));
            category.setDescription(categoryResult.getString(CATEGORY_DESCRIPTION_COLUMN_NAME));
        }
        categoryResult.close();
        getQuery.close();

        return category;
    }
}
