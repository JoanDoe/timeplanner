package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.descriptor.Descriptor;
import planner.descriptor.pagination.PaginationConfig;
import planner.descriptor.searchFilter.SearchCriteria;
import planner.descriptor.searchFilter.SearchKit;
import planner.descriptor.searchFilter.SearchingMode;
import planner.descriptor.sorting.SortKit;
import planner.descriptor.sorting.SortMode;
import planner.descriptor.sorting.SortingCriteria;
import planner.jersey.util.MetaTaskContent;
import planner.model.*;
import planner.util.UserInfo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * MetaTaskDaoImpl is the class implements PlannerDao interface for MetaTask model class.
 */
public class MetaTaskDaoImpl implements PlannerDao<MetaTask> {

    /**
     * A variable representing table name in DB containing instances of this class.
     */
    private final static String TABLE_NAME = "TASK";

    /**
     * A variable representing metatask id column name in DB.
     */
    private final static String METATASK_ID_COLUMN_NAME = "ID";

    /**
     * A variable representing metatask title column name in DB.
     */
    private final static String METATASK_TITLE_COLUMN_NAME = "TITLE";

    /**
     * A variable representing metatask description column name in DB.
     */
    private final static String METATASK_DESCRIPTION_COLUMN_NAME = "DESCRIPTION";

    /**
     * A variable representing metatask priority column name in DB.
     */
    private final static String METATASK_PRIORITY_COLUMN_NAME = "PRIORITY";

    /**
     * A variable representing metatask due date column name in DB.
     */
    private final static String METATASK_DUE_DATE_COLUMN_NAME = "DUE_DATE";

    /**
     * A variable representing metatask start date column name in DB.
     */
    private final static String METATASK_START_DATE_COLUMN_NAME = "START_DATE";

    /**
     * A variable representing metatask time scale column name in DB.
     */
    private final static String METATASK_TIME_SCALE_COLUMN_NAME = "TIME_SCALE";

    /**
     * A variable representing metatask completeness column name in DB.
     */
    private final static String METATASK_COMPLETENESS_COLUMN_NAME = "COMPLETENESS";

    /**
     * A variable representing metatask category column name in DB.
     */
    private final static String METATASK_CATEGORY_COLUMN_NAME = "CATEGORY";

    /**
     * A variable representing the only instance of this class.
     */
    private static MetaTaskDaoImpl instance;

    static {
        instance = new MetaTaskDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of MetaTaskDaoImpl class
     */
    public static MetaTaskDaoImpl getInstance() {
        return instance;
    }

    /**
     * A variable representing instance of TimeScaleDaoImpl class.
     */
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();

    /**
     * A variable representing instance of CategoryDaoImpl class.
     */
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();

    /**
     * A variable representing instance of TaskDaoImpl class.
     */
    TaskDaoImpl taskDao = TaskDaoImpl.getInstance();

    /**
     * The method used for adding some record in the table "task" in DB.
     *
     * @param metaTask the object which will be added in database
     */
    public void add(MetaTask metaTask) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement addTagQuery = null;
        PreparedStatement addTaskQuery = null;

        int taskAutoId;

        try {
            PreparedStatement addQuery = connection.prepareStatement("INSERT INTO task (title, description, priority, due_date, " +
                    "start_date, time_scale, completeness, owner, is_metatask,category) " +
                    "VALUES (?, ?, ?, ?, ?, ?, 'false', ?, 'true',?)", Statement.RETURN_GENERATED_KEYS);
            addQuery.setString(1, metaTask.getTitle());
            addQuery.setString(2, metaTask.getDescription());
            addQuery.setInt(3, metaTask.getPriority());
            addQuery.setDate(4, metaTask.getDueDate());
            addQuery.setDate(5, metaTask.getStartDate());
            addQuery.setInt(6, metaTask.getTimeScale().getId());
            addQuery.setString(7, UserInfo.getUserLogin());
            addQuery.setInt(8, metaTask.getCategory().getId());
            addQuery.executeUpdate();

            ResultSet rs = addQuery.getGeneratedKeys();
            rs.next();

            taskAutoId = rs.getInt(1);

            addQuery.close();

            addTagQuery = connection.prepareStatement("INSERT INTO task_tag (task_id, tag_id) VALUES (?,?)");
            for (Tag tag : metaTask.getTags()) {
                addTagQuery.setInt(1, taskAutoId);
                addTagQuery.setInt(2, tag.getId());
                addTagQuery.executeUpdate();
            }

            //client can add only existing tasks
            addTaskQuery = connection.prepareStatement("INSERT INTO task_task (parent_id, child_id) VALUES (?,?)");
            for (Task task : metaTask.getSubTasks()) {
                addTaskQuery.setInt(1, taskAutoId);
                addTaskQuery.setInt(2, task.getId());
                addTaskQuery.executeUpdate();
            }

            addTagQuery.close();
            addTaskQuery.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "task" in DB.
     *
     * @param metaTask the object which will be deleted from database
     */
    public void delete(MetaTask metaTask) {

        Connection connection = DBConnection.getConnection();
        try {
            PreparedStatement deleteQuery = connection.prepareStatement("DELETE FROM task WHERE ID=?;");
            deleteQuery.setInt(1, metaTask.getId());
            deleteQuery.executeUpdate();

            deleteQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting some record from the table "task" in DB.
     *
     * @param key the primary key of record which will be gotten
     * @return the object of the MetaTask class with set primary key
     */
    public MetaTask getById(int key) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task where id=?");
        getQuery.setInt(1, key);
        ResultSet taskResult = getQuery.executeQuery();

        MetaTask metaTask = new MetaTask();

        while (taskResult.next()) {
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));
        }

        taskResult.close();
        getQuery.close();

        return metaTask;
    }

    /**
     * The method used for getting all records from the table "task" in DB. Has no input parameters
     *
     * @return List of all MetaTask objects.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getAllQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (is_metatask=true) order by id");

        getAllQuery.setString(1, UserInfo.getUserLogin());
        ResultSet taskResult = getAllQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getAllQuery.close();

        return metaTasks;
    }


    /**
     * The method used for getting all records the table "task" in DB.
     *
     * @param descriptor the instance od Descriptor class containing information about paging, sorting and searching.
     * @return instance of MetaTaskContent class containing list of metatasks and metatasks quantity.
     */
    public MetaTaskContent getAll(Descriptor descriptor) throws SQLException {
        final int START_INDEX_DEFAULT_VALUE = 0;
        final String GET_ALL_TASKS_QUERY_HEADER = "SELECT distinct id,title,description,priority," +
                "due_date,start_date,time_scale,category,completeness FROM TASK  left outer JOIN task_tag ON task.id" +
                " = task_tag.task_id WHERE ((owner = ?) AND (is_metatask=true))";
        final String All_TASKS_QUANTITY_QUERY_HEADER = "SELECT count(distinct id) FROM TASK " +
                "left outer JOIN task_tag ON task.id = task_tag.task_id" +
                "  WHERE ((owner = ?) AND (is_metatask=true))";
        final String ORDER_BY = " order by ";
        final String LIMIT = " LIMIT ";
        final String CATEGORY_EXPR = METATASK_CATEGORY_COLUMN_NAME + " = ";
        final String TAG_EXPR = " task_tag.tag_id= ";
        final String SCALE_EXPR = METATASK_TIME_SCALE_COLUMN_NAME + " = ";
        final String COMPLETENESS_EXPR = METATASK_COMPLETENESS_COLUMN_NAME + " = ";
        final String AND = " and ";
        final String OR = " or ";

        MetaTaskContent metaTaskContent = new MetaTaskContent();

        StringBuilder dynamicQueryPart = new StringBuilder();

        int startIndex = START_INDEX_DEFAULT_VALUE;
        int offset = Integer.MAX_VALUE;
        PaginationConfig paginationConfig = descriptor.getPaginationConfig();

        if (paginationConfig != null) {
            startIndex = paginationConfig.getStartIndex();
            offset = paginationConfig.getOffset();
        }

        String limitValue = startIndex + "," + offset;

        SearchKit searchKit = descriptor.getSearchKit();
        boolean ifNotFirstCriteria = false;

        if (searchKit != null) {
            SearchCriteria searchCriteria = searchKit.getSearchCriteria();
            SearchingMode searchingMode = searchKit.getSearchingMode();

            if (searchCriteria != null) {
                Boolean completeness = searchCriteria.getCompleteness();
                Category category = searchCriteria.getCategory();
                List<Tag> tags = searchCriteria.getTags();
                TimeScale scale = searchCriteria.getTimeScale();
                String searchLogic = new String();
                if (searchingMode != null) {
                    if (searchingMode == SearchingMode.AND) {
                        searchLogic = AND;
                    } else {
                        searchLogic = OR;
                    }
                }
                if (completeness != null) {
                    dynamicQueryPart.append(AND + " ( " + COMPLETENESS_EXPR + completeness + " ) ");
                }
                if (category != null) {
                    ifNotFirstCriteria = true;
                    dynamicQueryPart.append(AND + "(" + CATEGORY_EXPR + category.getId());
                }
                if (scale != null) {
                    if (ifNotFirstCriteria == true) {
                        dynamicQueryPart.append(searchLogic + SCALE_EXPR + scale.getId());
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + SCALE_EXPR + scale.getId());
                    }
                }
                if (tags != null) {
                    if (ifNotFirstCriteria == true) {
                        for (Tag tag : tags) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tag.getId());
                        }
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + TAG_EXPR + tags.get(0).getId());
                        for (int i = 1; i < tags.size(); i++) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tags.get(i).getId());
                        }
                    }
                }
                dynamicQueryPart.append(")");
            }
        }

        SortKit sortKit = descriptor.getSortKit();
        String orderByValue = TABLE_NAME + "." + METATASK_ID_COLUMN_NAME;
        String sortWay = " ASC ";

        if (sortKit != null) {
            SortingCriteria sortingCriteria = sortKit.getSortingCriteria();
            SortMode sortMode = sortKit.getSortMode();

            if (sortingCriteria != null) {
                switch (sortingCriteria) {
                    case PRIORITY:
                        orderByValue = TABLE_NAME + "." + METATASK_PRIORITY_COLUMN_NAME;
                        break;
                    case CATEGORY:
                        orderByValue = TABLE_NAME + "." + METATASK_PRIORITY_COLUMN_NAME;
                        break;
                    case DUE_DATE:
                        orderByValue = TABLE_NAME + "." + METATASK_DUE_DATE_COLUMN_NAME;
                        break;
                    case TIME_SCALE:
                        orderByValue = TABLE_NAME + "." + METATASK_TIME_SCALE_COLUMN_NAME;
                        break;
                }
            }
            if (sortMode != null) {
                switch (sortMode) {
                    case DESC:
                        sortWay = " DESC ";
                        break;
                }
            }
        }

        PrintWriter writer = null;

        try {
            writer = new PrintWriter("log.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.println(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        writer.println(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        writer.close();

        Connection connection = DBConnection.getConnection();

        PreparedStatement getQuantityQuery = connection.prepareStatement(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        getQuantityQuery.setString(1, UserInfo.getUserLogin());
        ResultSet taskQuantityResult = getQuantityQuery.executeQuery();

        while (taskQuantityResult.next()) {
            metaTaskContent.setAllTasksQuantity(taskQuantityResult.getInt("count(distinct id)"));
        }

        taskQuantityResult.close();
        getQuantityQuery.close();

        PreparedStatement getAllQuery = connection.prepareStatement(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        getAllQuery.setString(1, UserInfo.getUserLogin());

        ResultSet taskResult = getAllQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getAllQuery.close();

        metaTaskContent.setMetaTasks(metaTasks);

        return metaTaskContent;
    }

    /**
     * The method used for getting all records from the table "task" in DB.
     *
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of MetaTask in set range.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAll(int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getAllQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (is_metatask=true) order by id LIMIT ?,?");

        getAllQuery.setString(1, UserInfo.getUserLogin());
        getAllQuery.setInt(2, startIndex);
        getAllQuery.setInt(3, offset);

        ResultSet taskResult = getAllQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getAllQuery.close();

        return metaTasks;
    }

    /**
     * The method used for update some record in the table "task" in DB.
     *
     * @param metaTask the object which will be updated in database
     */
    public void update(MetaTask metaTask) {
        Connection connection = DBConnection.getConnection();

        PreparedStatement updateTagQuery;
        PreparedStatement deleteTagsQuery;
        PreparedStatement updateTaskQuery;
        PreparedStatement deleteTaskQuery;

        try {
            PreparedStatement updateQuery = connection.prepareStatement("UPDATE task  SET  title=?, description=?," +
                    " priority=?, due_date=?, start_date=?, time_scale=?, " +
                    "completeness=?, owner=?,category=?, is_metatask = true  WHERE id = ?;");
            updateQuery.setString(1, metaTask.getTitle());
            updateQuery.setString(2, metaTask.getDescription());
            updateQuery.setInt(3, metaTask.getPriority());
            updateQuery.setDate(4, metaTask.getDueDate());
            updateQuery.setDate(5, metaTask.getStartDate());
            updateQuery.setInt(6, metaTask.getTimeScale().getId());
            updateQuery.setBoolean(7, metaTask.isCompleteness());
            updateQuery.setString(8, UserInfo.getUserLogin());
            updateQuery.setInt(9, metaTask.getCategory().getId());
            updateQuery.setInt(10, metaTask.getId());

            updateQuery.executeUpdate();
            updateQuery.close();

            deleteTagsQuery = connection.prepareStatement("DELETE FROM task_tag WHERE task_id=?");
            deleteTagsQuery.setInt(1, metaTask.getId());
            deleteTagsQuery.executeUpdate();
            deleteTagsQuery.close();

            updateTagQuery = connection.prepareStatement("INSERT INTO task_tag (task_id, tag_id) VALUES (?,?)");
            for (Tag tag : metaTask.getTags()) {
                updateTagQuery.setInt(1, metaTask.getId());
                updateTagQuery.setInt(2, tag.getId());
                updateTagQuery.executeUpdate();
            }

            deleteTaskQuery = connection.prepareStatement("DELETE FROM task_task WHERE parent_id=?");
            deleteTaskQuery.setInt(1, metaTask.getId());
            deleteTaskQuery.executeUpdate();
            deleteTaskQuery.close();

            //client can add only existing tasks
            updateTaskQuery = connection.prepareStatement("INSERT INTO task_task (parent_id, child_id) VALUES (?,?)");
            for (Task task : metaTask.getSubTasks()) {
                updateTaskQuery.setInt(1, metaTask.getId());
                updateTaskQuery.setInt(2, task.getId());
                updateTaskQuery.executeUpdate();
            }

            updateTagQuery.close();
            deleteTagsQuery.close();
            deleteTaskQuery.close();
            updateTaskQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting all records with particular tag from the table "task" in DB.
     *
     * @param tag value of particular tag
     * @return list of MetaTask with particular tag.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByTag(Tag tag) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task INNER JOIN task_tag ON task.id = task_tag.task_id " +
                "WHERE (task.owner =?) AND (task_tag.tag_id=?) AND (is_metatask=true) order by task.id");
        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, tag.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }

    /**
     * The method used for getting all records with particular tag from the table "task" in DB.
     *
     * @param tag        value of particular tag
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of MetaTask in set range and with particular tag.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByTag(Tag tag, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task INNER JOIN task_tag ON task.id = task_tag.task_id " +
                "WHERE (task.owner =?) AND (task_tag.tag_id=?) AND (is_metatask=true) order by task.id LIMIT ?,?");
        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, tag.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }

    /**
     * The method used for getting all records with particular category from the table "task" in DB.
     *
     * @param category value of particular category
     * @return list of MetaTask with particular category.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByCategory(Category category) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN category  ON task.category = category.id " +
                "WHERE (task.owner =?) AND (category.id=?) AND (is_metatask=true) order by task.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, category.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }

    /**
     * The method used for getting all records with particular category from the table "task" in DB.
     *
     * @param category   value of particular category
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of MetaTask in set range and with particular category.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByCategory(Category category, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN category  ON task.category = category.id " +
                "WHERE (task.owner =?) AND (category.id=?) AND (is_metatask=true) order by task.id LIMIT ?,?");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, category.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScaleDao.getById(taskResult.getInt(METATASK_TIME_SCALE_COLUMN_NAME)));
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }

    /**
     * The method used for getting all records with particular time scale from the table "task" in DB.
     *
     * @param timeScale value of particular time scale
     * @return list of MetaTask with particular time scale.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByTimeScale(TimeScale timeScale) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (time_scale = ?) AND (is_metatask=true) order by task.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, timeScale.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScale);
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }

    /**
     * The method used for getting all records with particular time scale from the table "task" in DB.
     *
     * @param timeScale  value of particular time scale
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of MetaTask in set range and with particular time scale.
     * @deprecated
     */
    @Deprecated
    public List<MetaTask> getAllByTimeScale(TimeScale timeScale, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (time_scale = ?) AND (is_metatask=true) order by task.id LIMIT ?,?");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, timeScale.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<MetaTask> metaTasks = new ArrayList<MetaTask>();

        while (taskResult.next()) {
            MetaTask metaTask = new MetaTask();
            metaTask.setId(taskResult.getInt(METATASK_ID_COLUMN_NAME));
            metaTask.setTitle(taskResult.getString(METATASK_TITLE_COLUMN_NAME));
            metaTask.setDescription(taskResult.getString(METATASK_DESCRIPTION_COLUMN_NAME));
            metaTask.setPriority(taskResult.getInt(METATASK_PRIORITY_COLUMN_NAME));
            metaTask.setDueDate(taskResult.getDate(METATASK_DUE_DATE_COLUMN_NAME));
            metaTask.setStartDate(taskResult.getDate(METATASK_START_DATE_COLUMN_NAME));
            metaTask.setTimeScale(timeScale);
            metaTask.setCategory(categoryDao.getById(taskResult.getInt(METATASK_CATEGORY_COLUMN_NAME)));
            metaTask.setCompleteness(taskResult.getBoolean(METATASK_COMPLETENESS_COLUMN_NAME));
            metaTask.setSubTasks(taskDao.getAllByMetaTask(metaTask));

            metaTasks.add(metaTask);
        }

        taskResult.close();
        getQuery.close();

        return metaTasks;
    }
}
