package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.model.MetaTask;
import planner.model.Tag;
import planner.model.Task;
import planner.util.UserInfo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * TagDaoImpl is the class implements PlannerDao interface for Tag model class.
 */
public class TagDaoImpl implements PlannerDao<Tag> {

    /**
     * A variable representing tag id column name in DB.
     */
    private final static String TAG_ID_COLUMN_NAME = "ID";

    /**
     * A variable representing tag name column name in DB.
     */
    private final static String TAG_NAME_COLUMN_NAME = "NAME";

    /**
     * A variable representing the only instance of this class.
     */
    private static TagDaoImpl instance;

    static {
        instance = new TagDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of TagDaoImpl class
     */
    public static TagDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in the table "tag" in DB.
     *
     * @param tag the object which will be added in database
     */
    public void add(Tag tag) {
        Connection connection = DBConnection.getConnection();

        PreparedStatement addQuery;
        try {
            addQuery = connection.prepareStatement("INSERT INTO tag (name, owner) VALUES (?, ?);");
            addQuery.setString(1, tag.getName());
            addQuery.setString(2, UserInfo.getUserLogin());
            addQuery.executeUpdate();
            addQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "tag" in DB.
     *
     * @param tag the object which will be deleted from database
     */
    public void delete(Tag tag) {
        Connection connection = DBConnection.getConnection();

        PreparedStatement deleteQuery;
        try {
            deleteQuery = connection.prepareStatement("DELETE FROM tag WHERE ID=?;");
            deleteQuery.setInt(1, tag.getId());
            deleteQuery.executeUpdate();
            deleteQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting some record from the table "tag" in DB.
     *
     * @param key the primary key of record which will be gotten
     * @return object of Tag class with set primary key
     * @throws SQLException
     */
    public Tag getById(int key) throws SQLException {
        Connection connection = DBConnection.getConnection();

        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM tag where id=?");
        getQuery.setInt(1, key);
        ResultSet tagResult = getQuery.executeQuery();

        Tag tag = new Tag();
        while (tagResult.next()) {
            tag.setId(tagResult.getInt(TAG_ID_COLUMN_NAME));
            tag.setName(tagResult.getString(TAG_NAME_COLUMN_NAME));
        }
        tagResult.close();
        getQuery.close();

        return tag;
    }

    /**
     * The method used for getting all records from the table "tag" in DB. Has no input parameters
     *
     * @return List of all MetaTask objects.
     * @throws SQLException
     */
    public List<Tag> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();

        PreparedStatement getAllQuery = connection.prepareStatement("SELECT * FROM tag where owner=? order by id");
        getAllQuery.setString(1, UserInfo.getUserLogin());
        ResultSet tagResult = getAllQuery.executeQuery();

        List<Tag> tags = new ArrayList<Tag>();
        while (tagResult.next()) {
            Tag tag = new Tag();
            tag.setId(tagResult.getInt(TAG_ID_COLUMN_NAME));
            tag.setName(tagResult.getString(TAG_NAME_COLUMN_NAME));
            tags.add(tag);
        }

        tagResult.close();
        getAllQuery.close();

        return tags;
    }

    /**
     * The method used for update some record in the table "tag" in DB.
     *
     * @param tag the object which will be updated in database
     */
    public void update(Tag tag) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement updateQuery;
        try {
            updateQuery = connection.prepareStatement("UPDATE tag  SET  name = ?, owner = ?  WHERE id = ?;");
            updateQuery.setString(1, tag.getName());
            updateQuery.setString(2, UserInfo.getUserLogin());
            updateQuery.setInt(3, tag.getId());

            updateQuery.executeUpdate();
            updateQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting all records with particular tag from the table "tag" in DB.
     *
     * @param task value of particular task
     * @return list of Tags with particular tag.
     * @throws SQLException
     */
    public List<Tag> getAllByTask(Task task) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM tag INNER JOIN task_tag ON tag.id = task_tag.tag_id " +
                "WHERE (tag.owner =?) AND (task_tag.task_id=?) order by tag.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, task.getId());
        ResultSet tagResult = getQuery.executeQuery();
        List<Tag> tags = new ArrayList<Tag>();
        while (tagResult.next()) {
            Tag tag = new Tag();
            tag.setId(tagResult.getInt(TAG_ID_COLUMN_NAME));
            tag.setName(tagResult.getString(TAG_NAME_COLUMN_NAME));
            tags.add(tag);
        }

        tagResult.close();
        getQuery.close();

        return tags;
    }

    /**
     * The method used for getting all records with particular tag from the table "tag" in DB.
     *
     * @param task value of particular task
     * @return list of Tags with particular tag.
     * @throws SQLException
     */
    public List<Tag> getAllByMeTaTask(MetaTask task) throws SQLException {
        //TODO test this method
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM tag INNER JOIN task_tag ON tag.id = task_tag.tag_id " +
                "WHERE (tag.owner =?) AND (task_tag.task_id=?) order by tag.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, task.getId());
        ResultSet tagResult = getQuery.executeQuery();
        List<Tag> tags = new ArrayList<Tag>();
        while (tagResult.next()) {
            Tag tag = new Tag();
            tag.setId(tagResult.getInt(TAG_ID_COLUMN_NAME));
            tag.setName(tagResult.getString(TAG_NAME_COLUMN_NAME));
            tags.add(tag);
        }

        tagResult.close();
        getQuery.close();

        return tags;
    }
}
