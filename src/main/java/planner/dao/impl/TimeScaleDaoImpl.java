package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.model.TimeScale;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * TimeScaleDaoImpl is the class implements PlannerDao interface for TimeScale model class.
 */
public class TimeScaleDaoImpl implements PlannerDao<TimeScale> {

    /**
     * A variable representing time scale id column name in DB.
     */
    private final static String TIME_SCALE_ID_COLUMN_NAME = "ID";

    /**
     * A variable representing time scale scale column name in DB.
     */
    private final static String TIME_SCALE_SCALE_COLUMN_NAME = "SCALE";

    /**
     * A variable representing the only instance of this class.
     */
    private static TimeScaleDaoImpl instance;

    static {
        instance = new TimeScaleDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of TimeScaleDaoImpl class
     */
    public static TimeScaleDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in the table "time_scale" in DB.
     *
     * @param timeScale the object which will be added in database
     */
    public void add(TimeScale timeScale) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement addQuery = null;
        try {
            addQuery = connection.prepareStatement("INSERT INTO timescale (scale) VALUES (?);");
            addQuery.setString(1, timeScale.getScale());

            addQuery.executeUpdate();
            addQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "time_scale" in DB.
     *
     * @param object the object which will be deleted from database
     */
    public void delete(TimeScale object) {
    }

    /**
     * The method used for getting some record from the table "time_scale" in DB.
     *
     * @param key the primary key of record which will be gotten
     * @return object of TimeScale class with set primary key
     */
    public TimeScale getById(int key) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM timescale where id=?");
        getQuery.setInt(1, key);
        ResultSet timeScaleResult = getQuery.executeQuery();

        TimeScale timeScale = new TimeScale();
        while (timeScaleResult.next()) {
            timeScale.setId(timeScaleResult.getInt(TIME_SCALE_ID_COLUMN_NAME));
            timeScale.setScale(timeScaleResult.getString(TIME_SCALE_SCALE_COLUMN_NAME));
        }
        timeScaleResult.close();
        getQuery.close();

        return timeScale;
    }

    /**
     * The method used for getting all records from the table "time_scale" in DB. Has no input parameters
     *
     * @return List of all TimeScale objects.
     * @throws SQLException
     */
    public List<TimeScale> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement getAllQuery = connection.createStatement();
        ResultSet timeScaleResult = getAllQuery.executeQuery("SELECT * FROM timescale order by id;");

        List<TimeScale> scales = new ArrayList<TimeScale>();

        while (timeScaleResult.next()) {
            TimeScale timeScale = new TimeScale();
            timeScale.setId(timeScaleResult.getInt(TIME_SCALE_ID_COLUMN_NAME));
            timeScale.setScale(timeScaleResult.getString(TIME_SCALE_SCALE_COLUMN_NAME));
            scales.add(timeScale);
        }

        timeScaleResult.close();
        getAllQuery.close();

        return scales;
    }

    /**
     * The method used for update some record in the table "time_scale" in DB.
     *
     * @param scale the object which will be updated in database
     */
    public void update(TimeScale scale) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement updateQuery = null;
        try {
            updateQuery = connection.prepareStatement("UPDATE timescale SET scale =? WHERE id=?");
            updateQuery.setString(1, scale.getScale());
            updateQuery.setInt(2, scale.getId());

            updateQuery.executeUpdate();
            updateQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
