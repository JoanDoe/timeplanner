package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.descriptor.Descriptor;
import planner.descriptor.pagination.PaginationConfig;
import planner.descriptor.searchFilter.SearchCriteria;
import planner.descriptor.searchFilter.SearchKit;
import planner.descriptor.searchFilter.SearchingMode;
import planner.descriptor.sorting.SortKit;
import planner.descriptor.sorting.SortMode;
import planner.descriptor.sorting.SortingCriteria;
import planner.jersey.util.TaskContent;
import planner.model.*;
import planner.util.UserInfo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//todo : do getAll for tasks which don't belong to metatask

/**
 * TaskDaoImpl is the class implements PlannerDao interface for Task model class.
 */
public class TaskDaoImpl implements PlannerDao<Task> {

    /**
     * A variable representing table name in DB containing instances of this class.
     */
    private final static String TABLE_NAME = "TASK";

    /**
     * A variable representing task id column name in DB.
     */
    private final static String TASK_ID_COLUMN_NAME = "ID";

    /**
     * A variable representing task title column name in DB.
     */
    private final static String TASK_TITLE_COLUMN_NAME = "TITLE";

    /**
     * A variable representing task description column name in DB.
     */
    private final static String TASK_DESCRIPTION_COLUMN_NAME = "DESCRIPTION";

    /**
     * A variable representing task priority column name in DB.
     */
    private final static String TASK_PRIORITY_COLUMN_NAME = "PRIORITY";

    /**
     * A variable representing task due date column name in DB.
     */
    private final static String TASK_DUE_DATE_COLUMN_NAME = "DUE_DATE";

    /**
     * A variable representing task start date column name in DB.
     */
    private final static String TASK_START_DATE_COLUMN_NAME = "START_DATE";

    /**
     * A variable representing task time scale column name in DB.
     */
    private final static String TASK_TIME_SCALE_COLUMN_NAME = "TIME_SCALE";

    /**
     * A variable representing task completeness column name in DB.
     */
    private final static String TASK_COMPLETENESS_COLUMN_NAME = "COMPLETENESS";

    /**
     * A variable representing task category column name in DB.
     */
    private final static String TASK_CATEGORY_COLUMN_NAME = "CATEGORY";

    /**
     * A variable representing instance of TimeScaleDaoImpl class.
     */
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();

    /**
     * A variable representing instance of CategoryDaoImpl class.
     */
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();

    /**
     * A variable representing the only instance of this class.
     */
    private static TaskDaoImpl instance;

    static {
        instance = new TaskDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of TaskDaoImpl class
     */
    public static TaskDaoImpl getInstance() {
        return instance;
    }

    /**
     * The method used for adding some record in the table "task" in DB.
     *
     * @param task the object which will be added in database
     */
    public void add(Task task) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement addQuery = null;
        PreparedStatement addTagQuery = null;
        int taskAutoId;
        try {
            addQuery = connection.prepareStatement("INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, " +
                    "owner, is_metatask,category) VALUES (?, ?, ?, ?, ?, ?, FALSE , ?, false,?);", Statement.RETURN_GENERATED_KEYS);

            addQuery.setString(1, task.getTitle());
            addQuery.setString(2, task.getDescription());
            addQuery.setInt(3, task.getPriority());
            addQuery.setDate(4, task.getDueDate());
            addQuery.setDate(5, task.getStartDate());
            addQuery.setInt(6, task.getTimeScale().getId());
            addQuery.setString(7, UserInfo.getUserLogin());
            addQuery.setInt(8, task.getCategory().getId());
            addQuery.executeUpdate();

            ResultSet rs = addQuery.getGeneratedKeys();
            rs.next();

            taskAutoId = rs.getInt(1);

            addQuery.close();

            addTagQuery = connection.prepareStatement("INSERT INTO task_tag (task_id, tag_id) VALUES (?,?)");
            for (Tag tag : task.getTags()) {
                addTagQuery.setInt(1, taskAutoId);
                addTagQuery.setInt(2, tag.getId());
                addTagQuery.executeUpdate();
            }
            addTagQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "task" in DB.
     *
     * @param task the object which will be deleted from database
     */
    public void delete(Task task) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement deleteQuery;
        try {
            deleteQuery = connection.prepareStatement("DELETE FROM task WHERE ID=?;");
            deleteQuery.setInt(1, task.getId());
            deleteQuery.executeUpdate();

            deleteQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting some record from the table "task" in DB.
     *
     * @param key the primary key of record which will be gotten
     * @return the object of the Task class with set primary key
     */
    public Task getById(int key) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task where id=?");
        getQuery.setInt(1, key);
        ResultSet taskResult = getQuery.executeQuery();

        Task task = new Task();

        while (taskResult.next()) {
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));
        }
        taskResult.close();
        getQuery.close();

        return task;
    }

    /**
     * The method used for getting all records with particular tag from the table "task" in DB.
     *
     * @param tag value of particular tag
     * @return list of Task with particular tag.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByTag(Tag tag) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task INNER JOIN task_tag ON task.id = task_tag.task_id " +
                "WHERE (task.owner =?) AND (task_tag.tag_id=?) AND (is_metatask=false) order by task.id ");
        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, tag.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records with particular tag from the table "task" in DB.
     *
     * @param tag        value of particular tag
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of Task in set range and with particular tag.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByTag(Tag tag, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task INNER JOIN task_tag ON task.id = task_tag.task_id " +
                "WHERE (task.owner =?) AND (task_tag.tag_id=?) AND (is_metatask=false) order by task.id LIMIT ?,?");
        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, tag.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records with particular category from the table "task" in DB.
     *
     * @param category value of particular category
     * @return list of Task with particular category.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByCategory(Category category) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN category  ON task.category = category.id " +
                "WHERE (task.owner =?) AND (category.id=?) AND (is_metatask=false) order by task.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, category.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records with particular category from the table "task" in DB.
     *
     * @param category   value of particular category
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of Task in set range and with particular category.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByCategory(Category category, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN category  ON task.category = category.id " +
                "WHERE (task.owner =?) AND (category.id=?) AND (is_metatask=false) order by task.id LIMIT ?,?");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, category.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records from the table "task" in DB. Has no input parameters
     *
     * @return List of all Task objects.
     */
    @Deprecated
    public List<Task> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getAllQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (is_metatask=false) order by task.id");

        getAllQuery.setString(1, UserInfo.getUserLogin());
        ResultSet taskResult = getAllQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getAllQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records the table "task" in DB.
     *
     * @param descriptor the instance od Descriptor class containing information about paging, sorting and searching.
     * @return instance of MetaTaskContent class containing list of tasks and tasks quantity.
     */
    public TaskContent getAll(Descriptor descriptor) throws SQLException {
        final int START_INDEX_DEFAULT_VALUE = 0;
        final String GET_ALL_TASKS_QUERY_HEADER = "SELECT distinct id,title,description,priority," +
                "due_date,start_date,time_scale,category,completeness FROM TASK  left outer JOIN task_tag ON task.id" +
                " = task_tag.task_id WHERE ((owner = ?) AND (is_metatask=false))";
        final String All_TASKS_QUANTITY_QUERY_HEADER = "SELECT count(distinct id) FROM TASK " +
                "left outer JOIN task_tag ON task.id = task_tag.task_id" +
                "  WHERE ((owner = ?) AND (is_metatask=false))";
        final String ORDER_BY = " order by ";
        final String LIMIT = " LIMIT ";
        final String CATEGORY_EXPR = TASK_CATEGORY_COLUMN_NAME + " = ";
        final String TAG_EXPR = " task_tag.tag_id= ";
        final String SCALE_EXPR = TASK_TIME_SCALE_COLUMN_NAME + " = ";
        final String COMPLETENESS_EXPR = TASK_COMPLETENESS_COLUMN_NAME + " = ";
        final String AND = " and ";
        final String OR = " or ";

        TaskContent taskContent = new TaskContent();

        StringBuilder dynamicQueryPart = new StringBuilder();

        int startIndex = START_INDEX_DEFAULT_VALUE;
        int offset = Integer.MAX_VALUE;
        PaginationConfig paginationConfig = descriptor.getPaginationConfig();
        if (paginationConfig != null) {
            startIndex = paginationConfig.getStartIndex();
            offset = paginationConfig.getOffset();
        }
        String limitValue = startIndex + "," + offset;


        SearchKit searchKit = descriptor.getSearchKit();
        boolean ifNotFirstCriteria = false;
        if (searchKit != null) {
            SearchCriteria searchCriteria = searchKit.getSearchCriteria();
            SearchingMode searchingMode = searchKit.getSearchingMode();
            if (searchCriteria != null) {
                Boolean completeness = searchCriteria.getCompleteness();
                Category category = searchCriteria.getCategory();
                List<Tag> tags = searchCriteria.getTags();
                TimeScale scale = searchCriteria.getTimeScale();
                String searchLogic = new String();
                if (searchingMode != null) {
                    if (searchingMode == SearchingMode.AND) {
                        searchLogic = AND;
                    } else {
                        searchLogic = OR;
                    }
                }
                if (completeness != null) {
                    dynamicQueryPart.append(AND + " ( " + COMPLETENESS_EXPR + completeness + " ) ");
                }
                if (category != null) {
                    ifNotFirstCriteria = true;
                    dynamicQueryPart.append(AND + "(" + CATEGORY_EXPR + category.getId());
                }
                if (scale != null) {
                    if (ifNotFirstCriteria == true) {
                        dynamicQueryPart.append(searchLogic + SCALE_EXPR + scale.getId());
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + SCALE_EXPR + scale.getId());
                    }
                }
                if (tags != null) {
                    if (ifNotFirstCriteria == true) {
                        for (Tag tag : tags) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tag.getId());
                        }
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + TAG_EXPR + tags.get(0).getId());
                        for (int i = 1; i < tags.size(); i++) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tags.get(i).getId());
                        }
                    }
                }
                dynamicQueryPart.append(")");
            }

        }


        SortKit sortKit = descriptor.getSortKit();
        String orderByValue = TABLE_NAME + "." + TASK_ID_COLUMN_NAME;
        String sortWay = " ASC ";
        if (sortKit != null) {
            SortingCriteria sortingCriteria = sortKit.getSortingCriteria();
            SortMode sortMode = sortKit.getSortMode();

            if (sortingCriteria != null) {
                switch (sortingCriteria) {
                    case PRIORITY:
                        orderByValue = TABLE_NAME + "." + TASK_PRIORITY_COLUMN_NAME;
                        break;
                    case CATEGORY:
                        orderByValue = TABLE_NAME + "." + TASK_CATEGORY_COLUMN_NAME;
                        break;
                    case DUE_DATE:
                        orderByValue = TABLE_NAME + "." + TASK_DUE_DATE_COLUMN_NAME;
                        break;
                    case TIME_SCALE:
                        orderByValue = TABLE_NAME + "." + TASK_TIME_SCALE_COLUMN_NAME;
                        break;
                }
            }
            if (sortMode != null) {
                switch (sortMode) {
                    case DESC:
                        sortWay = " DESC ";
                        break;
                }
            }
        }

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("log.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.println(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        writer.println(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        writer.close();

        Connection connection = DBConnection.getConnection();

        PreparedStatement getQuantityQuery = connection.prepareStatement(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        getQuantityQuery.setString(1, UserInfo.getUserLogin());
        ResultSet taskQuantityResult = getQuantityQuery.executeQuery();
        while (taskQuantityResult.next()) {
            taskContent.setAllTasksQuantity(taskQuantityResult.getInt("count(distinct id)"));
        }
        taskQuantityResult.close();
        getQuantityQuery.close();


        PreparedStatement getAllQuery = connection.prepareStatement(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        getAllQuery.setString(1, UserInfo.getUserLogin());

        ResultSet taskResult = getAllQuery.executeQuery();
        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }
        taskResult.close();
        getAllQuery.close();
        taskContent.setTasks(tasks);

        return taskContent;
    }

    /**
     * The method used for getting all records the table "task" in DB.
     *
     * @param descriptor the instance od Descriptor class containing information about paging, sorting and searching.
     * @return instance of MetaTaskContent class containing list of tasks and tasks quantity.
     */
    public TaskContent getAllParentless(Descriptor descriptor) throws SQLException {
        final int START_INDEX_DEFAULT_VALUE = 0;
        final String GET_ALL_TASKS_QUERY_HEADER = "SELECT distinct id,title,description,priority," +
                "due_date,start_date,time_scale,category,completeness FROM TASK  left outer join TASK_TASK on id =child_id left outer JOIN task_tag ON task.id" +
                " = task_tag.task_id WHERE ((owner = ?) AND (is_metatask=false) and child_id is null)";
        final String All_TASKS_QUANTITY_QUERY_HEADER = "SELECT count(distinct id) FROM TASK " +
                "left outer join TASK_TASK on id =child_id left outer JOIN task_tag ON task.id = task_tag.task_id" +
                "  WHERE ((owner = ?) AND (is_metatask=false) and child_id is null)";
        final String ORDER_BY = " order by ";
        final String LIMIT = " LIMIT ";
        final String CATEGORY_EXPR = TASK_CATEGORY_COLUMN_NAME + " = ";
        final String TAG_EXPR = " task_tag.tag_id= ";
        final String SCALE_EXPR = TASK_TIME_SCALE_COLUMN_NAME + " = ";
        final String COMPLETENESS_EXPR = TASK_COMPLETENESS_COLUMN_NAME + " = ";
        final String AND = " and ";
        final String OR = " or ";

        TaskContent taskContent = new TaskContent();

        StringBuilder dynamicQueryPart = new StringBuilder();

        int startIndex = START_INDEX_DEFAULT_VALUE;
        int offset = Integer.MAX_VALUE;
        PaginationConfig paginationConfig = descriptor.getPaginationConfig();
        if (paginationConfig != null) {
            startIndex = paginationConfig.getStartIndex();
            offset = paginationConfig.getOffset();
        }
        String limitValue = startIndex + "," + offset;


        SearchKit searchKit = descriptor.getSearchKit();
        boolean ifNotFirstCriteria = false;
        if (searchKit != null) {
            SearchCriteria searchCriteria = searchKit.getSearchCriteria();
            SearchingMode searchingMode = searchKit.getSearchingMode();

            if (searchCriteria != null) {
                Boolean completeness = searchCriteria.getCompleteness();
                Category category = searchCriteria.getCategory();
                List<Tag> tags = searchCriteria.getTags();
                TimeScale scale = searchCriteria.getTimeScale();
                String searchLogic = new String();
                if (searchingMode != null) {
                    if (searchingMode == SearchingMode.AND) {
                        searchLogic = AND;
                    } else {
                        searchLogic = OR;
                    }
                }
                if (completeness != null) {
                    dynamicQueryPart.append(AND + " ( " + COMPLETENESS_EXPR + completeness + " ) ");
                }
                if (category != null) {
                    ifNotFirstCriteria = true;
                    dynamicQueryPart.append(AND + "(" + CATEGORY_EXPR + category.getId());
                }
                if (scale != null) {
                    if (ifNotFirstCriteria == true) {
                        dynamicQueryPart.append(searchLogic + SCALE_EXPR + scale.getId());
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + SCALE_EXPR + scale.getId());
                    }
                }
                if (tags != null) {
                    if (ifNotFirstCriteria == true) {
                        for (Tag tag : tags) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tag.getId());
                        }
                    } else {
                        ifNotFirstCriteria = true;
                        dynamicQueryPart.append(AND + "(" + TAG_EXPR + tags.get(0).getId());
                        for (int i = 1; i < tags.size(); i++) {
                            dynamicQueryPart.append(searchLogic + TAG_EXPR + tags.get(i).getId());
                        }
                    }
                }
                dynamicQueryPart.append(")");
            }

        }


        SortKit sortKit = descriptor.getSortKit();
        String orderByValue = TABLE_NAME + "." + TASK_ID_COLUMN_NAME;
        String sortWay = " ASC ";
        if (sortKit != null) {
            SortingCriteria sortingCriteria = sortKit.getSortingCriteria();
            SortMode sortMode = sortKit.getSortMode();

            if (sortingCriteria != null) {
                switch (sortingCriteria) {
                    case PRIORITY:
                        orderByValue = TABLE_NAME + "." + TASK_PRIORITY_COLUMN_NAME;
                        break;
                    case CATEGORY:
                        orderByValue = TABLE_NAME + "." + TASK_CATEGORY_COLUMN_NAME;
                        break;
                    case DUE_DATE:
                        orderByValue = TABLE_NAME + "." + TASK_DUE_DATE_COLUMN_NAME;
                        break;
                    case TIME_SCALE:
                        orderByValue = TABLE_NAME + "." + TASK_TIME_SCALE_COLUMN_NAME;
                        break;
                }
            }
            if (sortMode != null) {
                switch (sortMode) {
                    case DESC:
                        sortWay = " DESC ";
                        break;
                }
            }
        }

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("log.txt", "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.println(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        writer.println(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        writer.close();

        Connection connection = DBConnection.getConnection();

        PreparedStatement getQuantityQuery = connection.prepareStatement(All_TASKS_QUANTITY_QUERY_HEADER + dynamicQueryPart);
        getQuantityQuery.setString(1, UserInfo.getUserLogin());
        ResultSet taskQuantityResult = getQuantityQuery.executeQuery();
        while (taskQuantityResult.next()) {
            taskContent.setAllTasksQuantity(taskQuantityResult.getInt("count(distinct id)"));
        }
        taskQuantityResult.close();
        getQuantityQuery.close();


        PreparedStatement getAllQuery = connection.prepareStatement(GET_ALL_TASKS_QUERY_HEADER + dynamicQueryPart + ORDER_BY + " " + orderByValue + " " + sortWay + LIMIT + limitValue);
        getAllQuery.setString(1, UserInfo.getUserLogin());

        ResultSet taskResult = getAllQuery.executeQuery();
        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }
        taskResult.close();
        getAllQuery.close();
        taskContent.setTasks(tasks);

        return taskContent;
    }

    /**
     * The method used for getting all records with particular time scale from the table "task" in DB.
     *
     * @param timeScale value of particular time scale
     * @return list of Task with particular time scale.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByTimeScale(TimeScale timeScale) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (time_scale = ?) AND (is_metatask=false) order by task.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, timeScale.getId());
        ResultSet taskResult = getQuery.executeQuery();
        CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScale);
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records with particular time scale from the table "task" in DB.
     *
     * @param timeScale  value of particular time scale
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of MetaTask in set range and with particular time scale.
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByTimeScale(TimeScale timeScale, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM task WHERE (owner = ?) AND (time_scale = ?) AND (is_metatask=false) order by task.id LIMIT ?,?");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, timeScale.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();
        CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScale);
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for update some record in the table "task" in DB..
     *
     * @param task the object which will be updated in database
     */
    public void update(Task task) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement updateQuery;
        PreparedStatement updateTagQuery;
        PreparedStatement deleteTagsQuery;

        try {
            updateQuery = connection.prepareStatement("UPDATE task  SET  title=?, description=?," +
                    " priority=?, due_date=?, start_date=?, time_scale=?, " +
                    "completeness=?, owner=?,category=? is_metatask = FALSE  WHERE id = ?;");
            updateQuery.setString(1, task.getTitle());
            updateQuery.setString(2, task.getDescription());
            updateQuery.setInt(3, task.getPriority());
            updateQuery.setDate(4, task.getDueDate());
            updateQuery.setDate(5, task.getStartDate());
            updateQuery.setInt(6, task.getTimeScale().getId());
            updateQuery.setBoolean(7, task.isCompleteness());
            updateQuery.setString(8, UserInfo.getUserLogin());
            updateQuery.setInt(9, task.getCategory().getId());
            updateQuery.setInt(10, task.getId());

            updateQuery.executeUpdate();
            updateQuery.close();

            deleteTagsQuery = connection.prepareStatement("DELETE FROM task_tag WHERE task_id=?");
            deleteTagsQuery.setInt(1, task.getId());
            deleteTagsQuery.executeUpdate();
            deleteTagsQuery.close();

            updateTagQuery = connection.prepareStatement("INSERT INTO task_tag (task_id, tag_id) VALUES (?,?)");
            for (Tag tag : task.getTags()) {
                updateTagQuery.setInt(1, task.getId());
                updateTagQuery.setInt(2, tag.getId());
                updateTagQuery.executeUpdate();
            }
            updateTagQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting all records with particular metatask from the table "task" in DB.
     *
     * @param metaTask value of metatask containing returned tasks
     * @return list of Task which are included in the set metatask
     */
    public List<Task> getAllByMetaTask(MetaTask metaTask) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN task_task ON task_task.child_id = task.id " +
                "WHERE (task.owner =?) AND (task_task.parent_id=?) AND (is_metatask=false) order by task.id");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, metaTask.getId());
        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for getting all records with particular metatask from the table "task" in DB.
     *
     * @param metaTask   value of metatask containing returned tasks
     * @param startIndex value of start index for paging
     * @param offset     value of offset for paging
     * @return list of Task which are included in the set metatask and are in the set range
     * @deprecated
     */
    @Deprecated
    public List<Task> getAllByMetaTask(MetaTask metaTask, int startIndex, int offset) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT  * FROM task INNER JOIN task_task ON task_task.child_id = task.id " +
                "WHERE (task.owner =?) AND (task_task.parent_id=?) AND (is_metatask=false) order by task.id LIMIT ?,?");

        getQuery.setString(1, UserInfo.getUserLogin());
        getQuery.setInt(2, metaTask.getId());
        getQuery.setInt(3, startIndex);
        getQuery.setInt(4, offset);

        ResultSet taskResult = getQuery.executeQuery();

        List<Task> tasks = new ArrayList<Task>();

        while (taskResult.next()) {
            Task task = new Task();
            task.setId(taskResult.getInt(TASK_ID_COLUMN_NAME));
            task.setTitle(taskResult.getString(TASK_TITLE_COLUMN_NAME));
            task.setDescription(taskResult.getString(TASK_DESCRIPTION_COLUMN_NAME));
            task.setPriority(taskResult.getInt(TASK_PRIORITY_COLUMN_NAME));
            task.setDueDate(taskResult.getDate(TASK_DUE_DATE_COLUMN_NAME));
            task.setStartDate(taskResult.getDate(TASK_START_DATE_COLUMN_NAME));
            task.setTimeScale(timeScaleDao.getById(taskResult.getInt(TASK_TIME_SCALE_COLUMN_NAME)));
            task.setCategory(categoryDao.getById(taskResult.getInt(TASK_CATEGORY_COLUMN_NAME)));
            task.setCompleteness(taskResult.getBoolean(TASK_COMPLETENESS_COLUMN_NAME));

            tasks.add(task);
        }

        taskResult.close();
        getQuery.close();

        return tasks;
    }

    /**
     * The method used for searching records with particular criteria from the table "task" in DB with the AND searck logic.
     *
     * @param tasks          list in which searching will be
     * @param searchCriteria value if search criteria
     * @return list of Task, result of searching
     * @deprecated
     */
    @Deprecated
    public List<Task> searchAND(List<Task> tasks, SearchCriteria searchCriteria) {
        Category category = searchCriteria.getCategory();
        List<Tag> tags = searchCriteria.getTags();
        TimeScale timeScale = searchCriteria.getTimeScale();
        List<Task> filteredTasks = new ArrayList<Task>();
        Boolean taskContainsAllTags = true;
        try {
            for (Task task : tasks) {

                if (category != null) {
                    if (!task.getCategory().equals(category)) {
                        continue;
                    }
                }

                if (timeScale != null) {
                    if (!task.getTimeScale().equals(timeScale)) {
                        continue;
                    }
                }
                if (tags != null) {
                    for (Tag tag : tags) {
                        if (!task.getTags().contains(tag)) {
                            taskContainsAllTags = false;
                        }
                    }
                }
                if (taskContainsAllTags) {
                    filteredTasks.add(task);
                } else
                    taskContainsAllTags = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filteredTasks;
    }

    /**
     * The method used for searching records with particular criteria from the table "task" in DB with the OR searck logic.
     *
     * @param tasks          list in which searching will be
     * @param searchCriteria value if search criteria
     * @return list of Task, result of searching
     * @deprecated
     */
    @Deprecated
    public List<Task> searchOR(List<Task> tasks, SearchCriteria searchCriteria) {
        Category category = searchCriteria.getCategory();
        List<Tag> tags = searchCriteria.getTags();
        TimeScale timeScale = searchCriteria.getTimeScale();
        List<Task> filteredTasks = new ArrayList<Task>();
        try {
            for (Task task : tasks) {

                if (category != null) {
                    if (task.getCategory().equals(category)) {
                        filteredTasks.add(task);
                        continue;
                    }
                }

                if (timeScale != null) {
                    if (task.getTimeScale().equals(timeScale)) {
                        filteredTasks.add(task);
                        continue;
                    }
                }
                if (tags != null) {
                    for (Tag tag : tags) {
                        if (task.getTags().contains(tag)) {
                            filteredTasks.add(task);
                            break;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filteredTasks;
    }
}
