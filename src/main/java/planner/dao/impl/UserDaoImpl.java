package planner.dao.impl;

import planner.DBConnection.DBConnection;
import planner.dao.PlannerDao;
import planner.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * UserDaoImpl is the class implements PlannerDao interface for User model class.
 */
public class UserDaoImpl {

    /**
     * A variable representing the only instance of this class.
     */
    private static UserDaoImpl instance;

    static
    {
        instance = new UserDaoImpl();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of UserDaoImpl class
     */
    public static UserDaoImpl getInstance() {
        return instance;
    }

    /**
     * A variable representing user first name column name in DB.
     */
    private final static String USER_FIRST_NAME_COLUMN_NAME="FIRST_NAME";

    /**
     * A variable representing user last name column name in DB.
     */
    private final static String USER_LAST_NAME_COLUMN_NAME="LAST_NAME";

    /**
     * A variable representing user e-mail column name in DB.
     */
    private final static String USER_EMAIL_COLUMN_NAME="EMAIL";

    /**
     * A variable representing user password column name in DB.
     */
    private final static String USER_PASSWORD_COLUMN_NAME="PASSWORD";

    /**
     * A variable representing user login column name in DB.
     */
    private final static String USER_LOGIN_COLUMN_NAME="LOGIN";

    /**
     * The method used for adding some record in the table "user" in DB.
     *
     * @param user the object which will be added in database
     */
    public void add(User user) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement addQuery;
        try {
            addQuery = connection.prepareStatement("INSERT INTO USER ( first_name,  last_name, email, password, login) VALUES (?, ?, ?, ?, ?);");
            addQuery.setString(1, user.getFirstName());
            addQuery.setString(2, user.getLastName());
            addQuery.setString(3, user.getEMail());
            addQuery.setString(4, user.getPassword());
            addQuery.setString(5, user.getLogin());

            addQuery.executeUpdate();
            addQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for deleting some record from the table "user" in DB.
     *
     * @param user the object which will be deleted from database
     */
    public void delete(User user) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement deleteQuery;
        try {
            deleteQuery = connection.prepareStatement("DELETE FROM user WHERE login=?;");
            deleteQuery.setString(1, user.getLogin());
            deleteQuery.executeUpdate();
            deleteQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting some record from the table "user" in DB.
     *
     * @param login the primary key of record which will be gotten
     * @return object of User class with set primary key
     * @throws SQLException
     */
    public User getByLogin(String login) throws SQLException {
        Connection connection = DBConnection.getConnection();
        PreparedStatement getQuery = connection.prepareStatement("SELECT * FROM user WHERE login=?");
        getQuery.setString(1, login);
        ResultSet userResult = getQuery.executeQuery();

        User user = new User();
        while (userResult.next()) {
            user.setFirstName(userResult.getString(USER_FIRST_NAME_COLUMN_NAME));
            user.setLastName(userResult.getString(USER_LAST_NAME_COLUMN_NAME));
            user.setEMail(userResult.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(userResult.getString(USER_PASSWORD_COLUMN_NAME));
            user.setLogin(userResult.getString(USER_LOGIN_COLUMN_NAME));
        }
        userResult.close();
        getQuery.close();

        return user;
    }

    /**
     * The method used for getting all records from the table "user" in DB. Has no input parameters
     *
     * @return List of all User objects
     * @throws SQLException
     */
    public List<User> getAll() throws SQLException {
        Connection connection = DBConnection.getConnection();
        Statement getAllQuery = connection.createStatement();
        ResultSet userResult = getAllQuery.executeQuery("SELECT * FROM user order by login;");

        List<User> users = new ArrayList<User>();


        while (userResult.next()) {
            User user = new User();
            user.setFirstName(userResult.getString(USER_FIRST_NAME_COLUMN_NAME));
            user.setLastName(userResult.getString(USER_LAST_NAME_COLUMN_NAME));
            user.setEMail(userResult.getString(USER_EMAIL_COLUMN_NAME));
            user.setPassword(userResult.getString(USER_PASSWORD_COLUMN_NAME));
            user.setLogin(userResult.getString(USER_LOGIN_COLUMN_NAME));

            users.add(user);
        }

        userResult.close();
        getAllQuery.close();

        return users;
    }

    /**
     * The method used for update some record in the table "user" in DB.
     *
     * @param user the object which will be updated in database
     */
    public void update(User user) {
        Connection connection = DBConnection.getConnection();
        PreparedStatement updateQuery;
        try {
            updateQuery = connection.prepareStatement("UPDATE user SET first_name=?,  last_name=?, email=?, password=?, login=? WHERE login=?");
            updateQuery.setString(1, user.getFirstName());
            updateQuery.setString(2, user.getLastName());
            updateQuery.setString(3, user.getEMail());
            updateQuery.setString(4, user.getPassword());
            updateQuery.setString(5, user.getLogin());
            updateQuery.setString(6, user.getLogin());

            updateQuery.executeUpdate();
            updateQuery.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //todo : add javadoc
    public boolean verifyUser(String[] userInfo) {
        Connection connection = DBConnection.getConnection();

        long usersQuantity = 0;
        PreparedStatement verifyQuery;
        try {
            verifyQuery = connection.prepareStatement("SELECT COUNT(*) FROM user WHERE login=? AND password =?;");
            verifyQuery.setString(1, userInfo[0]);
            verifyQuery.setString(2, userInfo[1]);
            ResultSet userResult = verifyQuery.executeQuery();


            while (userResult.next()) {
                usersQuantity = userResult.getLong("COUNT(*)");
            }
            userResult.close();
            verifyQuery.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (usersQuantity == 0) {
            return false;
        } else {
            return true;
        }
    }
}
