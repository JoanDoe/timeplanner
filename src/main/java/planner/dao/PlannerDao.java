package planner.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * PlannerDao is the interface which provide all operations with database table records.
 */
public interface PlannerDao <T> {
	/**
	 * The method used for adding some record in database table.
	 *
	 * @param object the object which will be added in database
	 */
	void add(T object) throws SQLException;

	/**
	 * The method used for deleting some record from database table.
	 *
	 * @param object the object which will be deleted in database
	 */
	void delete(T object) throws SQLException;

	/**
	 * The method used for getting some record from database table.
	 *
	 * @param key the primary key of record which will be gotten
	 * @return T object, where T is the type of object stored as record in database.
	 */
	T getById(int key) throws SQLException;

	/**
	 * The method used for getting all records from database table. Has no input parameters
	 *
	 * @return List of T objects, where T is the type of objects stored as records in database.
	 */
	List<T> getAll() throws SQLException;

	/**
	 * The method used for update some record in database table.
	 *
	 * @param object the object which will be updated in database
	 */
	void update(T object) throws SQLException;
}
