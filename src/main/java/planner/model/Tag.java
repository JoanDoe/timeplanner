package planner.model;

import java.io.Serializable;

/**
 * Task is the class extending Task class. MetaTask can contain other tasks.
 *
 * @see Task
 */
public class Tag implements Serializable {

    /**
     * A variable representing unique identifier of tag.
     */
    private int id;

    /**
     * A variable representing name of tag.
     */
    private String name;

//    /**
//     * A variable representing tasks with this tag.
//     */
//    private List<Task> tasks;

    /**
     * The method used for getting unique identifier.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of name field.
     *
     * @return the string contains tag name
     */
    public String getName() {
        return name;
    }

    /**
     * The method used for setting value of name field.
     *
     * @param name the value of name field which will be set.
     */
    public void setName(String name) {
        this.name = name;
    }

//    /**
//     * The method used for getting value of tasks field.
//     *
//     * @return the Set value contains tasks
//     */
//    public List<Task> getTasks() throws SQLException {
////        TaskDaoImpl taskDao = new TaskDaoImpl();
////        return taskDao.getAllByTag(this);
//        return new ArrayList<Task>();
//    }

//    /**
//     * The method used for setting value of tasks field.
//     *
//     * @param tasks the value of tasks field which will be set.
//     */
//    public void setTasks(List<Task> tasks) {
//        this.tasks = tasks;
//    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof Tag) {
            Tag other = (Tag) object;
            if (((Tag) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                    & (other.getName().equals(this.getName()))
                    ) {
                return true;
            }
        }
        return false;
    }
}
