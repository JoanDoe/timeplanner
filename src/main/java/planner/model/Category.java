package planner.model;

import java.io.Serializable;

/**
 * Category is the class representing a set of tasks with common feature.
 *
 * @see Task
 */
public class Category implements Serializable {

    /**
     * A variable representing unique identifier of category.
     */
    private int id;

    /**
     * A variable representing name of category.
     */
    private String name;

    /**
     * A variable representing description of category.
     */
    private String description;

//    /**
//     * A variable representing set of tasks in the category.
//     */
//    private List<Task> tasks;

    /**
     * The method used for getting unique identifier.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of name field.
     *
     * @return the string contains category name
     */
    public String getName() {
        return name;
    }

    /**
     * The method used for setting value of name field.
     *
     * @param name the value of name field which will be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * The method used for getting value of description field.
     *
     * @return the string contains category description
     */
    public String getDescription() {
        return description;
    }

    /**
     * The method used for setting value of description field.
     *
     * @param description the value of description field which will be set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

//    /**
//     * The method used for getting value of tasks field.
//     *
//     * @return the set of Task objects contains category tasks
//     */
//    public List<Task> getTasks() throws SQLException {
////        TaskDaoImpl taskDao = TaskDaoImpl.getInstance();
////        return taskDao.getAllByCategory(this);
//        return new ArrayList<Task>();
//    }

//    /**
//     * The method used for setting value of tasks field.
//     *
//     * @param tasks the value of tasks field which will be set.
//     */
//    public void setTasks(List<Task> tasks) {
//        this.tasks = tasks;
//    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof Category) {
            Category other = (Category) object;
            if (((Category) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                    & (other.getName().equals(this.getName()))
                    ) {
                return true;
            }
        }
        return false;
    }
}
