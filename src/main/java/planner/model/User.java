package planner.model;

import java.io.Serializable;

/**
 * Task is the class representing a user of task, category and tag.
 */
public class User implements Serializable {

    /**
     * A variable representing first name of user.
     */
    private String firstName;

    /**
     * A variable representing last name of user.
     */
    private String lastName;

    /**
     * A variable representing user login.
     */
    private String login;

    /**
     * A variable representing first user e-mail.
     */
    private String eMail;

    /**
     * A variable representing first user password.
     */
    private String password;

    /**
     * The method used for getting value of first name field.
     *
     * @return the string contains user first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * The method used for setting value of firstName field.
     *
     * @param firstName the value of firstName field which will be set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * The method used for getting value of last name field.
     *
     * @return the string contains user last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * The method used for setting value of lastName field.
     *
     * @param lastName the value of lastName field which will be set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * The method used for getting value of login field.
     *
     * @return the string contains user login
     */
    public String getLogin() {
        return login;
    }

    /**
     * The method used for setting value of login field.
     *
     * @param login the value of login field which will be set.
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * The method used for getting value of eMail field.
     *
     * @return the string contains user e-mail
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * The method used for setting value of eMail field.
     *
     * @param eMail the value of eMail field which will be set.
     */
    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    /**
     * The method used for getting value of password field.
     *
     * @return the int value contains user password
     */
    public String getPassword() {
        return password;
    }

    /**
     * The method used for setting value of password field.
     *
     * @param password the value of password field which will be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    //todo add equals method
}
