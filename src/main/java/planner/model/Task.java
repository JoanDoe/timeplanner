package planner.model;

import planner.dao.impl.TagDaoImpl;

import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

/**
 * Task is the class representing a sequence of actions for achieving some goal.
 *
 * @see Category
 * @see Tag
 * @see TimeScale
 */
public class Task implements Serializable {

    /**
     * A variable representing unique identifier of task.
     */
    private int id;

    /**
     * A variable representing title of task.
     */
    private String title;

    /**
     * A variable representing description of task.
     */
    private String description;

    /**
     * A variable representing category that task contains.
     */
    private Category category;

    /**
     * A variable representing priority of task.
     */
    private int priority;

    /**
     * A variable representing start date of task.
     */
    private Date startDate;

    /**
     * A variable representing end date of task.
     */
    private Date dueDate;

    /**
     * A variable representing time scale of task.
     */
    private TimeScale timeScale;

    /**
     * A variable representing completeness of task.
     */
    private boolean completeness;

    /**
     * A variable representing set of tags of task.
     */
    private List<Tag> tags;

    /**
     * The method used for getting unique identifier.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of title field.
     *
     * @return the string contains task title
     */
    public String getTitle() {
        return title;
    }

    /**
     * The method used for setting value of title field.
     *
     * @param title the value of title field which will be set.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * The method used for getting value of description field.
     *
     * @return the string contains task description
     */
    public String getDescription() {
        return description;
    }

    /**
     * The method used for setting value of description field.
     *
     * @param description the value of description field which will be set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * The method used for getting value of category field.
     *
     * @return the set contains task category
     */
    public Category getCategory() {
//        CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();
//        return categoryDao.getByTask(this);
        return category;
    }

    /**
     * The method used for setting value of category field.
     *
     * @param category the value of category field which will be set.
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * The method used for getting value of priority field.
     *
     * @return the int value contains task priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * The method used for setting value of priority field.
     *
     * @param priority the value of priority field which will be set.
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    /**
     * The method used for getting value of startDate field.
     *
     * @return the Date value contains task start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * The method used for setting value of startDate field.
     *
     * @param startDate the value of startDate field which will be set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * The method used for getting value of dueDate field.
     *
     * @return the Date value contains task end date
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * The method used for setting value of dueDate field.
     *
     * @param dueDate the value of dueDate field which will be set.
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    /**
     * The method used for getting value of timeScale field.
     *
     * @return the instance of TimeTask class contains task time scale
     */
    public TimeScale getTimeScale() {
        return timeScale;
    }

    /**
     * The method used for setting value of timeScale field.
     *
     * @param timeScale the value of timeScale field which will be set.
     */
    public void setTimeScale(TimeScale timeScale) {
        this.timeScale = timeScale;
    }

    /**
     * The method used for getting value of completeness field.
     *
     * @return the boolean value contains task completeness
     */
    public boolean isCompleteness() {
        return completeness;
    }

    /**
     * The method used for setting value of completeness field.
     *
     * @param completeness the value of completeness field which will be set.
     */
    public void setCompleteness(boolean completeness) {
        this.completeness = completeness;
    }

    /**
     * The method used for getting value of tags field.
     *
     * @return the Set value contains task tags
     */
    public List<Tag> getTags() throws SQLException {
        //todo: get tags rest edition
        TagDaoImpl tagDao = TagDaoImpl.getInstance();
        return tagDao.getAllByTask(this);
    }

    /**
     * The method used for setting value of tags field.
     *
     * @param tags the value of tags field which will be set.
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof Task) {
            Task other = (Task) object;
            if (((Task) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                    & (other.getTitle().equals(this.getTitle()))
                    ) {
                return true;
            }
        }
        return false;
    }
}
