package planner.model;

import planner.dao.impl.TagDaoImpl;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * Task is the class extending Task class. MetaTask can contain other tasks.
 *
 * @see Task
 */
public class MetaTask extends Task implements Serializable {

    /**
     * A variable representing set of task that metatask contains.
     */
    private List<Task> subTasks;

    /**
     * The method used for getting value of subTasks field.
     *
     * @return the set value contains tasks
     */
    public List<Task> getSubTasks() throws SQLException {
//        TaskDaoImpl taskDao = TaskDaoImpl.getInstance();
        return subTasks;
    }

    /**
     * The method used for getting value of tags field.
     *
     * @return the Set value contains task tags
     */
    @Override
    public List<Tag> getTags() throws SQLException {
        //todo: get tags rest edition
        TagDaoImpl tagDao = TagDaoImpl.getInstance();
        return tagDao.getAllByMeTaTask(this);
    }

    /**
     * The method used for setting value of subTasks field.
     *
     * @param subTasks the value of subTasks field which will be set.
     */
    public void setSubTasks(List<Task> subTasks) {
        this.subTasks = subTasks;
    }

    //todo: add equals method �� ����������
}
