package planner.model;

import java.io.Serializable;

/**
 * Task is the class extending Task class. MetaTask can contain other tasks.
 *
 * @see Task
 */
public class TimeScale implements Serializable {

    /**
     * A variable representing unique identifier of task.
     */
    private int id;

    /**
     * A variable representing scale period.
     */
    private String scale;

//    /**
//     * A variable representing tasks of time scale.
//     */
//    private List<Task> tasks;

    /**
     * The method used for getting unique identifier.
     *
     * @return the int value contains id
     */
    public int getId() {
        return id;
    }

    /**
     * The method used for setting value of id field.
     *
     * @param id the value of id field which will be set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * The method used for getting value of scale field.
     *
     * @return the string contains scale period
     */
    public String getScale() {
        return scale;
    }

    /**
     * The method used for setting value of scale field.
     *
     * @param scale the value of scale field which will be set.
     */
    public void setScale(String scale) {
        this.scale = scale;
    }

//    /**
//     * The method used for getting value of tasks field.
//     *
//     * @return the Set value contains tasks
//     */
//    public List<Task> getTasks() throws SQLException {
////        TaskDaoImpl taskDao = new TaskDaoImpl();
////        return taskDao.getAllByTimeScale(this);
//        return new ArrayList<Task>();
//    }

//    /**
//     * The method used for setting value of tasks field.
//     *
//     * @param tasks the value of tasks field which will be set.
//     */
//    public void setTasks(List<Task> tasks) {
//        this.tasks = tasks;
//    }

    /**
     * The method used for equals instance of this class object and other object.
     *
     * @param object the object for comparing.
     * @return boolean value
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }

        if (object instanceof TimeScale) {
            TimeScale other = (TimeScale) object;
            if (((TimeScale) object).getClass().getSimpleName().equals(this.getClass().getSimpleName())
                    & (other.getScale()).equals(this.getScale())
                    ) {
                return true;
            }
        }
        return false;
    }
}
