package planner.jersey;

import planner.dao.impl.CategoryDaoImpl;
import planner.dao.impl.TagDaoImpl;
import planner.dao.impl.TaskDaoImpl;
import planner.dao.impl.TimeScaleDaoImpl;
import planner.descriptor.Descriptor;
import planner.descriptor.pagination.PaginationConfig;
import planner.descriptor.searchFilter.SearchCriteria;
import planner.descriptor.searchFilter.SearchKit;
import planner.descriptor.searchFilter.SearchingMode;
import planner.descriptor.sorting.SortKit;
import planner.descriptor.sorting.SortMode;
import planner.descriptor.sorting.SortingCriteria;
import planner.jersey.util.TaskContent;
import planner.model.Task;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;

/**
 * TaskResource is the class provides server response to rest client request about Task objects.
 */
@Path("/tasks")
public class TaskResource {

    /**
     * A variable representing instance of class TaskDaoImpl. TaskDaoImpl provides access to the table "task".
     */
    TaskDaoImpl taskDao = TaskDaoImpl.getInstance();

    /**
     * The method used for getting all tasks from DB through http method "GET".
     * URL of this resource is "/tasks".
     *
     * @return the list of all Tasks objects
     * @deprecated
     */
    @Deprecated
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Task> getAllTasks() {
        List<Task> tasks = null;
        try {
            tasks = taskDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tasks;
    }

    /**
     * The method used for getting all tasks from DB through http method "POST".
     * URL of this resource is "/tasks/filter".
     *
     * @param descriptor the Descriptor object containing searching, sorting and paging parameters
     * @return the TaskContent object containing list of tasks and all tasks quantity
     */
    @POST
    @Path("/filter")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TaskContent getAllTasks(Descriptor descriptor) {
        //TODO solve null pointer problem
        TaskContent taskContent = null;
        try {
            taskContent = taskDao.getAll(descriptor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskContent;
    }

    /**
     * The method used for getting all parentless tasks from DB through http method "POST".
     * URL of this resource is "/tasks/filter/parentless".
     *
     * @param descriptor the Descriptor object containing searching, sorting and paging parameters
     * @return the TaskContent object containing list of tasks and all tasks quantity
     */
    @POST
    @Path("/filter/parentless")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TaskContent getAllParentless(Descriptor descriptor) {
        //TODO solve null pointer problem
        TaskContent taskContent = null;
        try {
            taskContent = taskDao.getAllParentless(descriptor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskContent;
    }

    // @Deprecated
    @POST
    @Path("/test")
    @Produces(MediaType.APPLICATION_JSON)
    public Descriptor test() {
        Descriptor descriptor = new Descriptor();

        SortKit sortKit = new SortKit();
        sortKit.setSortingCriteria(SortingCriteria.TIME_SCALE);
        sortKit.setSortMode(SortMode.ASC);
        descriptor.setSortKit(sortKit);

        PaginationConfig paginationConfig = new PaginationConfig();
        paginationConfig.setStartIndex(0);
        paginationConfig.setOffset(99);
        descriptor.setPaginationConfig(paginationConfig);

        SearchKit searchKit = new SearchKit();
        SearchCriteria searchCriteria = new SearchCriteria();
        SearchingMode searchingMode = SearchingMode.AND;
        searchKit.setSearchingMode(searchingMode);
        CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();
        try {
            searchCriteria.setCategory(categoryDao.getById(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TagDaoImpl tagDao = TagDaoImpl.getInstance();
        try {
            searchCriteria.setTags(tagDao.getAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();
        try {
            searchCriteria.setTimeScale(timeScaleDao.getById(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Boolean compl = true;
        searchCriteria.setCompleteness(compl);
        searchKit.setSearchCriteria(searchCriteria);
        descriptor.setSearchKit(searchKit);

        return descriptor;
    }

    /**
     * The method used for getting task with certain primary key from DB through http method "GET".
     * URL of this resource is "/tasks/{id}".
     *
     * @param id the primary key of task which will be gotten
     * @return the Task object with certain primary key
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Task getTaskById(@PathParam("id") int id) {
        Task task = null;
        try {
            task = taskDao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return task;
    }

    /**
     * The method used for adding task to DB through http method "POST".
     * URL of this resource is "/tasks".
     *
     * @param task the Task object which will be added
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTask(Task task) {
        taskDao.add(task);
    }

    /**
     * The method used for updating task in DB through http method "PUT".
     * URL of this resource is "/tasks".
     *
     * @param task the Task object which will be updated
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTask(Task task) {
        taskDao.update(task);
    }

    /**
     * The method used for deleting task from DB through http method "DELETE".
     * URL of this resource is "/tasks".
     *
     * @param task the Task object which will be deleted
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTask(Task task) {
        taskDao.delete(task);
    }
}
