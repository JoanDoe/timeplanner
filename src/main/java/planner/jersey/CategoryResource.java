package planner.jersey;

import planner.dao.impl.CategoryDaoImpl;
import planner.model.Category;
import planner.model.Task;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;

/**
 * CategoryResource is the class provides server response to rest client request about Category objects.
 */
@Path("/categories")
public class CategoryResource {

    /**
     * A variable representing instance of class CategoryDaoImpl. CategoryDaoImpl provides access to the table "category".
     */
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();

    /**
     * The method used for getting all categories from DB through http method "GET".
     * URL of this resource is "/categories".
     *
     * @return the list of Category objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Category> getAllCategories() {
        List<Category> categories = null;
        try {
            categories = categoryDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    /**
     * The method used for getting category with certain task from DB through http method "POST".
     * URL of this resource is "/categories/byTask".
     *
     * @param task the instance of Task class containing certain task
     * @return the Category object with certain task
     */
    @POST
    @Path("/byTask")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Category getAllByTask(Task task) {
        Category category = null;
        try {
            category = categoryDao.getByTask(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }

    /**
     * The method used for getting category with certain primary key from DB through http method "GET".
     * URL of this resource is "/categories/{id}".
     *
     * @param id the primary key of category which will be gotten
     * @return the Category object with certain primary key
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Category getCategoryById(@PathParam("id") int id) {
        Category category = null;
        try {
            category = categoryDao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }

    /**
     * The method used for adding category to DB through http method "POST".
     * URL of this resource is "/categories".
     *
     * @param category the Category object which will be added
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addCategory(Category category) {
        categoryDao.add(category);
    }

    /**
     * The method used for updating category in DB through http method "PUT".
     * URL of this resource is "/categories".
     *
     * @param category the Category object which will be updated
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCategory(Category category) {
        categoryDao.update(category);
    }

    /**
     * The method used for deleting category from DB through http method "DELETE".
     * URL of this resource is "/categories".
     *
     * @param category the Category object which will be deleted
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteCategory(Category category) {
        categoryDao.delete(category);
    }
}
