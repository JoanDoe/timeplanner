package planner.jersey;

import planner.dao.impl.TagDaoImpl;
import planner.model.Tag;
import planner.model.Task;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;

/**
 * TagResource is the class provides server response to rest client request about Tag objects.
 */
@Path("/tags")
public class TagResource {

    /**
     * A variable representing instance of class TagDaoImpl. TagDaoImpl provides access to the table "tag".
     */
    TagDaoImpl tagDao = TagDaoImpl.getInstance();

    /**
     * The method used for getting all tags from DB through http method "GET".
     * URL of this resource is "/tags".
     *
     * @return the list of Tag objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tag> getAllTags() {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }

    /**
     * The method used for getting tags with certain task from DB through http method "POST".
     * URL of this resource is "/tags/byTask".
     *
     * @param task the instance of Task class containing certain task
     * @return the list of Tag objects with certain task
     */
    @POST
    @Path("/byTask")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tag> getAllByTask(Task task) {
        List<Tag> tags = null;
        try {
            tags = tagDao.getAllByTask(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tags;
    }

    /**
     * The method used for getting tag with certain primary key from DB through http method "GET".
     * URL of this resource is "/tags/{id}".
     *
     * @param id the primary key of tag which will be gotten
     * @return the Tag object with certain primary key
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tag getTagById(@PathParam("id") int id) {
        Tag tag = null;
        try {
            tag = tagDao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tag;
    }

    /**
     * The method used for adding tag to DB through http method "POST".
     * URL of this resource is "/tags".
     *
     * @param tag the Tag object which will be added
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTag(Tag tag) {
        tagDao.add(tag);
    }

    /**
     * The method used for updating tag in DB through http method "PUT".
     * URL of this resource is "/tags".
     *
     * @param tag the Tag object which will be updated
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTag(Tag tag) {
        tagDao.update(tag);
    }

    /**
     * The method used for deleting tag from DB through http method "DELETE".
     * URL of this resource is "/tags".
     *
     * @param tag the Tag object which will be deleted
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTag(Tag tag) {
        tagDao.delete(tag);
    }
}
