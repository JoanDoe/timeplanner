package planner.jersey.auth;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import planner.dao.impl.UserDaoImpl;
import planner.model.User;
import planner.util.UserInfo;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

/**
 * AuthFilter is the class provides authorization checking.
 */
public class AuthFilter implements ContainerRequestFilter {
    /**
     * Apply the filter : check input request, validate or not with user auth
     *
     * @param containerRequest The request from Tomcat server
     */
    public ContainerRequest filter(ContainerRequest containerRequest) throws WebApplicationException {
        UserDaoImpl userDao = UserDaoImpl.getInstance();

        String path = containerRequest.getPath(true);
        String method = containerRequest.getMethod();

        //Get the authentification passed in HTTP headers parameters
        String auth = containerRequest.getHeaderValue("authorization");
        if (path.equals("users") & method.equals("POST")) {
            return containerRequest;
        } else
            //If the user does not have the right (does not provide any HTTP Basic Auth)
            if (auth == null) {
                throw new WebApplicationException(Response.Status.UNAUTHORIZED);
            }

        //lap : loginAndPassword
        String[] lap = BasicAuth.decode(auth);

        //If login or password fail
        if (lap == null || lap.length != 2) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        if (!userDao.verifyUser(lap)) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

        User user;
        try {
            user = userDao.getByLogin(lap[0]);
            UserInfo.setUser(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return containerRequest;
    }
}
