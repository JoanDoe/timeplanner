package planner.jersey;

import planner.dao.impl.TimeScaleDaoImpl;
import planner.model.TimeScale;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.List;

//todo: ask about right name
/**
 * TimeScaleResource is the class provides server response to rest client request about TimeScale objects.
 */
@Path("/timeScales")
public class TimeScaleResource {

    /**
     * A variable representing instance of class TimeScaleDaoImpl. TimeScaleDaoImpl provides access to the table "time_scale".
     */
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();

    /**
     * The method used for getting all time scales from DB through http method "GET".
     * URL of this resource is "/timeScales".
     *
     * @return the list of all TimeScale objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TimeScale> getAllTimeScales() {
        List<TimeScale> timeScales = null;
        try {
            timeScales = timeScaleDao.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timeScales;
    }

    /**
     * The method used for getting tine scale with certain primary key from DB through http method "GET".
     * URL of this resource is "/timeScales/{id}".
     *
     * @param id the primary key of time scale which will be gotten
     * @return the TimeScale object with certain primary key
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public TimeScale getTimeScaleById(@PathParam("id") int id) {
        TimeScale timeScale = null;
        try {
            timeScale = timeScaleDao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timeScale;
    }
}
