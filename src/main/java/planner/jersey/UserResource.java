package planner.jersey;

import planner.dao.impl.UserDaoImpl;
import planner.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;

/**
 * UserResource is the class provides server response to rest client request about User objects.
 */
@Path("/users")
public class UserResource {

    /**
     * A variable representing instance of class UserDaoImpl. UserDaoImpl provides access to the table "user".
     */
    UserDaoImpl userDao = UserDaoImpl.getInstance();

    /**
     * The method used for getting user with certain login (primary key) from DB through http method "GET".
     * URL of this resource is "/users/{login}".
     *
     * @param login the primary key of user which will be gotten
     * @return the User object with certain primary key (login)
     */
    @GET
    @Path("/{login}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUserByLogin(@PathParam("login") String login) {

        User user = null;
        try {
            user = userDao.getByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * The method used for adding user to DB through http method "POST".
     * URL of this resource is "/users".
     *
     * @param user the User object which will be added
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addUser(User user) {
        userDao.add(user);
    }
}
