package planner.jersey.util;

import planner.model.MetaTask;

import java.util.List;

/**
 * MetaTaskContent is the class containing list of some metatasks and all metatasks quantity.
 */
public class MetaTaskContent {

    /**
     * A variable representing list of metatasks.
     */
    private List<MetaTask> metaTasks;

    /**
     * A variable representing quantity of metatasks.
     */
    private int allTasksQuantity;

    /**
     * The method used for getting value of metaTasks field.
     *
     * @return the list of some MetaTask objects
     */
    public List<MetaTask> getMetaTasks() {
        return this.metaTasks;
    }

    /**
     * The method used for setting value of metaTasks field.
     *
     * @param metaTasks the value of metaTasks field which will be set.
     */
    public void setMetaTasks(List<MetaTask> metaTasks) {
        this.metaTasks = metaTasks;
    }

    /**
     * The method used for getting value of allTasksQuantity field.
     *
     * @return the int value representing all metatasks quantity
     */
    public int getAllTasksQuantity() {
        return allTasksQuantity;
    }

    /**
     * The method used for setting value of allTasksQuantity field.
     *
     * @param allTasksQuantity the value of allTasksQuantity field which will be set.
     */
    public void setAllTasksQuantity(int allTasksQuantity) {
        this.allTasksQuantity = allTasksQuantity;
    }

}
