package planner.jersey.util;

import planner.model.Task;

import java.util.List;

/**
 * TaskContent is the class containing list of some tasks and all tasks quantity.
 */
public class TaskContent {

    /**
     * A variable representing list of tasks.
     */
    private List<Task> tasks;

    /**
     * A variable representing quantity of all tasks.
     */
    private int allTasksQuantity;

    /**
     * The method used for getting value of tasks field.
     *
     * @return the list of some Task objects
     */
    public List<Task> getTasks() {
        return this.tasks;
    }

    /**
     * The method used for setting value of tasks field.
     *
     * @param tasks the value of tasks field which will be set.
     */
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    /**
     * The method used for getting value of allTasksQuantity field.
     *
     * @return the int value representing all tasks quantity
     */
    public int getAllTasksQuantity() {
        return allTasksQuantity;
    }

    /**
     * The method used for setting value of allTasksQuantity field.
     *
     * @param allTasksQuantity the value of allTasksQuantity field which will be set.
     */
    public void setAllTasksQuantity(int allTasksQuantity) {
        this.allTasksQuantity = allTasksQuantity;
    }
}
