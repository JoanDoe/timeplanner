package planner.jersey;

import planner.dao.impl.MetaTaskDaoImpl;
import planner.descriptor.Descriptor;
import planner.jersey.util.MetaTaskContent;
import planner.model.MetaTask;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;

/**
 * MetaTaskResource is the class provides server response to rest client request about MetaTask objects.
 */
@Path("/metaTasks")
public class MetaTaskResource {

    /**
     * A variable representing instance of class MetaTaskDaoImpl. MetaTaskDaoImpl provides access to the table "task".
     */
    MetaTaskDaoImpl metaTaskDao = MetaTaskDaoImpl.getInstance();

    /**
     * The method used for getting metatask with certain primary key from DB through http method "GET".
     * URL of this resource is "/metaTasks/{id}".
     *
     * @param id the primary key of metatask which will be gotten
     * @return the MetaTask object with certain primary key
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public MetaTask getTaskById(@PathParam("id") int id) {
        MetaTask task = null;
        try {
            task = metaTaskDao.getById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return task;
    }

    /**
     * The method used for getting all metatasks from DB through http method "POST".
     * URL of this resource is "/metaTasks/filter".
     *
     * @param descriptor the Descriptor object containing searching, sorting and paging parameters
     * @return the MetaTaskContent object containing list of metatasks and all metatasks quantity
     */
    @POST
    @Path("/filter")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MetaTaskContent getAllTasks(Descriptor descriptor) {
        //TODO solve null pointer problem
        MetaTaskContent metaTaskContent = null;
        try {
            metaTaskContent = metaTaskDao.getAll(descriptor);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return metaTaskContent;
    }

    /**
     * The method used for adding metatask to DB through http method "POST".
     * URL of this resource is "/metaTasks".
     *
     * @param task the MetaTask object which will be added
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addTask(MetaTask task) {
        metaTaskDao.add(task);
    }

    /**
     * The method used for updating metatask in DB through http method "PUT".
     * URL of this resource is "/metaTasks".
     *
     * @param task the MetaTask object which will be updated
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateTask(MetaTask task) {
        metaTaskDao.update(task);
    }

    /**
     * The method used for deleting metatask from DB through http method "DELETE".
     * URL of this resource is "/metaTasks".
     *
     * @param task the MetaTask object which will be deleted
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTask(MetaTask task) {
        metaTaskDao.delete(task);
    }
}
