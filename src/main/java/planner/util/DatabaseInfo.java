package planner.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * DatabaseInfo is the class containing necessary for database connection information.
 */
public class DatabaseInfo {

    /**
     * A final static variable representing properties file name for database connection.
     */
    private final static String PROPERTIES_PATH = "userH2Data.properties";

    /**
     * A variable representing database user login.
     */
    private String login;

    /**
     * A variable representing database user password.
     */
    private String password;

    /**
     * A variable representing database URL.
     */
    private String URL;

    /**
     * A default constructor for this class. It uses database connection parameters drom properties file.
     */
    public DatabaseInfo() {
        Properties properties = new Properties();
        InputStream inputStream = null;

        try {
            inputStream = DatabaseInfo.class.getClassLoader().getResourceAsStream(PROPERTIES_PATH);
            if (inputStream == null) {
                System.out.println("Sorry, unable to find " + PROPERTIES_PATH);
                return;
            }

            properties.load(inputStream);

            this.login = properties.getProperty("login");
            this.password = properties.getProperty("password");
            this.URL = properties.getProperty("URL");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * The method used for getting value of login field.
     *
     * @return the string contains login
     */
    public String getLogin() {
        return login;
    }

    /**
     * The method used for getting value of password field.
     *
     * @return the string contains password
     */
    public String getPassword() {
        return password;
    }

    /**
     * The method used for getting value of URL field.
     *
     * @return the string contains URL
     */
    public String getURL() {
        return URL;
    }

}

