package planner.util;

import planner.dao.impl.UserDaoImpl;
import planner.model.User;

import java.sql.SQLException;

/**
 * DatabaseInfo is the class containing information about the current user.
 */
public class UserInfo {

    /**
     * A static variable representing instance of User class.
     */
    private static User user;

    /**
     * A variable representing instance of class UserDaoImpl. UserDaoImpl provides access to the table "user".
     */
    private static UserDaoImpl userDao = UserDaoImpl.getInstance();

    static {
        try {
            //todo: delete this user cap in release
            user = userDao.getByLogin("login");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for getting value of user login.
     *
     * @return the string contains user login
     */
    public static String getUserLogin() {
        return user.getLogin();
    }

    /**
     * The method used for getting current User object.
     *
     * @return the instance of User class
     */
    public static User getUser() {

        try {
            user = userDao.getByLogin(getUserLogin());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * The method used for setting value of user field.
     *
     * @param newUser the value of user field which will be set.
     */
    public static void setUser(User newUser) {
        user = newUser;
    }
}