package planner.DBConnection;

import planner.util.DatabaseInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DBConnection is the class provides connection with database.
 */
public class DBConnection {

    /**
     * A variable representing instance of class Connection.
     */
    private static Connection connection;

    /**
     * A variable representing instance of class DatabaseInfo containing database URL, user login and password.
     */
    private static DatabaseInfo databaseInfo = new DatabaseInfo();

    /**
     * The method used for creating connection with database through DriverManager.
     */
    private static void createConnection() {
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(databaseInfo.getURL(), databaseInfo.getLogin(), databaseInfo.getPassword());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method used for closing connection with database.
     */
    public static void closeConnection() throws SQLException {
        getConnection().close();
    }

    /**
     * The method used for getting the only instance of this class.
     *
     * @return the object of Connection class
     */
    public static Connection getConnection() {
        if (connection == null) {
            createConnection();
        }
        return connection;
    }

}
