package planner.descriptor;

import planner.descriptor.pagination.PaginationConfig;
import planner.descriptor.searchFilter.SearchKit;
import planner.descriptor.sorting.SortKit;
import planner.model.Task;

/**
 * Descriptor is the class describing searching, sorting and paging parameters.
 */
public class Descriptor {

    /**
     * A variable representing instance of class SortKit.
     */
    private SortKit sortKit;

    /**
     * A variable representing instance of class SearchKit.
     */
    private SearchKit searchKit;

    /**
     * A variable representing instance of class PaginationConfig.
     */
    private PaginationConfig paginationConfig;

    /**
     * The method used for getting value of sortKit field.
     *
     * @return the SortKit object
     */
    public SortKit getSortKit() {
        return sortKit;
    }

    /**
     * The method used for setting value of sortKit field.
     *
     * @param sortKit the value of sortKit field which will be set.
     */
    public void setSortKit(SortKit sortKit) {
        this.sortKit = sortKit;
    }

    /**
     * The method used for getting value of paginationConfig field.
     *
     * @return the PaginationConfig object
     */
    public PaginationConfig getPaginationConfig() {
        return paginationConfig;
    }

    /**
     * The method used for setting value of paginationConfig field.
     *
     * @param paginationConfig the value of paginationConfig field which will be set.
     */
    public void setPaginationConfig(PaginationConfig paginationConfig) {
        this.paginationConfig = paginationConfig;
    }

    /**
     * The method used for getting value of searchKit field.
     *
     * @return the SearchKit object
     */
    public SearchKit getSearchKit() {
        return searchKit;
    }

    /**
     * The method used for setting value of searchKit field.
     *
     * @param searchKit the value of searchKit field which will be set.
     */
    public void setSearchKit(SearchKit searchKit) {
        this.searchKit = searchKit;
    }
}
