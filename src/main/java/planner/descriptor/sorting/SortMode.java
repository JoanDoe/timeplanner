package planner.descriptor.sorting;

/**
 * SortMode is the enum containing sorting mode (descending or ascending).
 */
public enum SortMode {
    ASC,
    DESC
}
