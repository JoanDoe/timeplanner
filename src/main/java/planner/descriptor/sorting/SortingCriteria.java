package planner.descriptor.sorting;

/**
 * SortingCriteria is the enum containing parameters on which tasks will be sorted.
 */
public enum SortingCriteria {
    ID, PRIORITY, CATEGORY, TIME_SCALE, DUE_DATE
}