package planner.descriptor.sorting;

/**
 * SortKit is the class containing sorting criteria and mode.
 */
public class SortKit {

    /**
     * A variable representing instance of class SortMode.
     */
    private SortMode sortMode;

    /**
     * A variable representing instance of class SortingCriteria.
     */
    private SortingCriteria sortingCriteria;

    /**
     * The method used for getting value of sortMode field.
     *
     * @return the SortMode object
     */
    public SortMode getSortMode() {
        return sortMode;
    }

    /**
     * The method used for setting value of sortMode field.
     *
     * @param sortMode the value of sortMode field which will be set.
     */
    public void setSortMode(SortMode sortMode) {
        this.sortMode = sortMode;
    }

    /**
     * The method used for getting value of sortingCriteria field.
     *
     * @return the SortingCriteria object
     */
    public SortingCriteria getSortingCriteria() {
        return sortingCriteria;
    }

    /**
     * The method used for setting value of sortingCriteria field.
     *
     * @param sortingCriteria the value of sortingCriteria field which will be set.
     */
    public void setSortingCriteria(SortingCriteria sortingCriteria) {
        this.sortingCriteria = sortingCriteria;
    }
}
