package planner.descriptor.pagination;

/**
 * PaginationConfig is the class containing configuration of paging.
 */
public class PaginationConfig {

    /**
     * A variable representing first element on page index.
     */
    private int startIndex;

    /**
     * A variable representing quantity of elements on page.
     */
    private int offset;

    /**
     * The method used for getting value of start index on page.
     *
     * @return the int value contains start index
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * The method used for setting value of start index on page.
     *
     * @param startIndex the value of startIndex field which will be set.
     */
    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * The method used for getting value of offset on page.
     *
     * @return the int value contains offset
     */
    public int getOffset() {
        return offset;
    }

    /**
     * The method used for setting value of offset.
     *
     * @param offset the value of offset field which will be set.
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }
}
