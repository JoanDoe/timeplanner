package planner.descriptor.searchFilter;

import planner.model.Category;
import planner.model.Tag;
import planner.model.TimeScale;

import java.util.List;

/**
 * SearchCriteria is the class contains parameters which will be searched.
 */
public class SearchCriteria {

    /**
     * A variable representing list of tags which will be searched.
     */
    private List<Tag> tags;

    /**
     * A variable representing category which will be searched.
     */
    private Category category;

    /**
     * A variable representing time scale which will be searched.
     */
    private TimeScale timeScale;

    private Boolean completeness;

    /**
     * The method used for getting value of tags field.
     *
     * @return the list contains tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * The method used for setting value of tags field.
     *
     * @param tags the value of tags field which will be set.
     */
    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    /**
     * The method used for getting value of category field.
     *
     * @return the Category object
     */
    public Category getCategory() {
        return category;
    }

    /**
     * The method used for setting value of category field.
     *
     * @param category the value of category field which will be set.
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * The method used for getting value of timeScale field.
     *
     * @return the TimeScale object
     */
    public TimeScale getTimeScale() {
        return timeScale;
    }

    /**
     * The method used for setting value of timeScale field.
     *
     * @param timeScale the value of timeScale field which will be set.
     */
    public void setTimeScale(TimeScale timeScale) {
        this.timeScale = timeScale;
    }

    /**
     * The method used for getting value of completeness field.
     *
     * @return the Boolean object
     */
    public Boolean getCompleteness() {
        return completeness;
    }


    /**
     * The method used for setting value of completeness field.
     *
     * @param completeness the value of Boolean field which will be set.
     */
    public void setCompleteness(Boolean completeness) {
        this.completeness = completeness;
    }
}
