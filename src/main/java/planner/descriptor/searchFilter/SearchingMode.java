package planner.descriptor.searchFilter;

/**
 * SearchingMode is the enum containing searching logic ("or" or "and").
 */
public enum SearchingMode {
    OR, AND
}
