package planner.descriptor.searchFilter;

/**
 * SearchKit is the class containing searching criteria and logic mode.
 */
public class SearchKit {

    /**
     * A variable representing instance of class SearchCriteria.
     */
    private SearchCriteria searchCriteria;

    /**
     * A variable representing instance of class SearchingMode.
     */
    private SearchingMode searchingMode;

    /**
     * The method used for getting value of searchCriteria field.
     *
     * @return the SearchCriteria object
     */
    public SearchCriteria getSearchCriteria() {
        return searchCriteria;
    }

    /**
     * The method used for setting value of searchCriteria field.
     *
     * @param searchCriteria the value of searchCriteria field which will be set.
     */
    public void setSearchCriteria(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    /**
     * The method used for getting value of searchingMode field.
     *
     * @return the SearchingMode object
     */
    public SearchingMode getSearchingMode() {
        return searchingMode;
    }

    /**
     * The method used for setting value of searchingMode field.
     *
     * @param searchingMode the value of searchingMode field which will be set.
     */
    public void setSearchingMode(SearchingMode searchingMode) {
        this.searchingMode = searchingMode;
    }
}
