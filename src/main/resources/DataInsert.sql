INSERT INTO user ( first_name,  last_name, email, password, login) VALUES ('default', 'user', 'test@mail.com', '1', 'login');
INSERT INTO user ( first_name,  last_name, email, password, login) VALUES ('hop', 'hey', 'lalaley@mail.com', '23', 'user2');

INSERT INTO category (name, description, owner) VALUES ('sport', 'gym exercises, nutriment, trainer advi�es', 'login');
INSERT INTO category (name, description, owner) VALUES ('university', 'laboratory works, course work, diploma', 'login');
INSERT INTO category (name, description, owner) VALUES ('rest', 'meets with friends and relatives, goings to cinema and theatres', 'login');
INSERT INTO category (name, description, owner) VALUES ('housework', 'cleaning and repairs', 'login');
INSERT INTO category (name, description, owner) VALUES ('job', 'work like a dog', 'login');
INSERT INTO category (name, description, owner) VALUES ('holydays', 'drink like a dog', 'login');



INSERT INTO tag (name, owner) VALUES ('sport nutriment', 'login' );
INSERT INTO tag (name, owner) VALUES ('exercises' , 'login');
INSERT INTO tag (name, owner) VALUES ('trainer' , 'login');
INSERT INTO tag (name, owner) VALUES ('progress' , 'login');
INSERT INTO tag (name, owner) VALUES ('course work' , 'login');
INSERT INTO tag (name, owner) VALUES ('laboratory work' , 'login');
INSERT INTO tag (name, owner) VALUES ('diploma' , 'login');
INSERT INTO tag (name, owner) VALUES ('company f friends' , 'login');
INSERT INTO tag (name, owner) VALUES ('movies' , 'login');
INSERT INTO tag (name, owner) VALUES ('birthdate' , 'login');
INSERT INTO tag (name, owner) VALUES ('cleaning' , 'login');
INSERT INTO tag (name, owner) VALUES ('repair' , 'login');
INSERT INTO tag (name, owner) VALUES ('repair' , 'user2');
INSERT INTO tag (name, owner) VALUES ('football' , 'user2');
INSERT INTO tag (name, owner) VALUES ('boring' , 'user2');


INSERT INTO timescale (scale) VALUES ('year' );
INSERT INTO timescale (scale) VALUES ('month' );
INSERT INTO timescale (scale) VALUES ('week' );
INSERT INTO timescale (scale) VALUES ('day' );

INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('bench press', 'learn to do right technique', '2', '2016-02-27', '2016-01-31', '2', 'false', 'login', 'false','1');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('proteins in nutriment', 'count quantity of proteins in nutriment', '2', '2016-02-20', '2016-01-31', '2', 'false', 'login', 'true','2');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('buy the season ticket', 'buy the season ticket in alexfitness ', '3', '2016-03-15', '2016-01-31', '3', 'false', 'login', 'false','2');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('android development', 'install "android studio"', '1', '2016-02-08', '2016-01-31', '2', 'false', 'login', 'false','4');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('moms birthday', 'paint the giftbox', '1', '2016-02-14', '2016-01-31', '3', 'false', 'login', 'false','3');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('power socket', 'repair power socket in the kitchen', '1', '2016-06-01', '2016-01-31', '2', 'false', 'login', 'false','1');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('gym', 'training int the gym', '1', '2016-06-01', '2016-01-31', '2', 'false', 'login', 'true','5');
INSERT INTO task (title, description, priority, due_date, start_date, time_scale, completeness, owner, is_metatask,category) VALUES ('ULC', 'watch it all', '1', '2016-02-14', '2016-06-01', '2', 'false', 'user2', 'false','5');



INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('1', '1' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('1', '4' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('2', '1' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('3', '2' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('3', '4' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('6', '12' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('4', '7' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('5', '10' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('7', '2' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('7', '3' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('7', '4' );
INSERT INTO TASK_TAG (task_id, tag_id) VALUES ('8', '13' );

INSERT INTO TASK_TASK  (parent_id, child_id) VALUES ('7', '1' );
INSERT INTO TASK_TASK  (parent_id, child_id) VALUES ('7', '2' );
INSERT INTO TASK_TASK  (parent_id, child_id) VALUES ('7', '3' );
INSERT INTO TASK_TASK  (parent_id, child_id) VALUES ('2', '4' );
INSERT INTO TASK_TASK  (parent_id, child_id) VALUES ('2', '5' );