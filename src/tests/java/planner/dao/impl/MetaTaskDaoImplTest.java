package planner.dao.impl;

import org.junit.Ignore;
import org.junit.Test;
import planner.model.Category;
import planner.model.MetaTask;
import planner.model.Task;
import planner.model.TimeScale;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by �� on 2/9/2016.
 */
public class MetaTaskDaoImplTest {
    MetaTaskDaoImpl metaTaskDao = MetaTaskDaoImpl.getInstance();
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();
    UserDaoImpl userDao = UserDaoImpl.getInstance();
    TagDaoImpl tagDao = TagDaoImpl.getInstance();
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();
    TaskDaoImpl taskDao = TaskDaoImpl.getInstance();

    @Test
    @Ignore
    public void testAdd() throws Exception {
        System.out.println("add");
        MetaTask metaTask = new MetaTask();
        metaTask.setTitle("test setting tasks");
        metaTask.setDescription("added for tests");
        metaTask.setPriority(5);

        Date dueDate = new Date(999966699313123l);
        metaTask.setDueDate(dueDate);

        Date startDate = new Date(999343532552l);
        metaTask.setStartDate(startDate);

        TimeScale timeScale = timeScaleDao.getById(1);
        metaTask.setTimeScale(timeScale);

        Category category = categoryDao.getById(1);
        metaTask.setCategory(category);

        metaTask.setCompleteness(false);
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(taskDao.getById(6));
        tasks.add(taskDao.getById(15));
        metaTask.setSubTasks(tasks);

        metaTaskDao.add(metaTask);
    }

    @Test
    @Ignore
    public void testDelete() throws Exception {
        MetaTask task = metaTaskDao.getById(16);
        metaTaskDao.delete(task);
    }

    @Test
    public void testGetById() throws Exception {
        MetaTask task = metaTaskDao.getById(7);
        System.out.println("GetByID");
        System.out.println(task.getId() + "   " + task.getTitle() + " " +
                task.getDescription() + "   " + task.getPriority() + "  " +
                task.getDueDate().toString() + "    " + task.getStartDate() +
                " " + task.isCompleteness());
    }

    @Ignore

    @Test
    public void testGetAllByTag() throws Exception {
        List<MetaTask> tasks = metaTaskDao.getAllByTag(tagDao.getById(2));
        System.out.println("\nGetAllByTag");
        for (MetaTask task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore

    public void testGetAllByCategory() throws Exception {
        List<MetaTask> tasks = metaTaskDao.getAllByCategory(categoryDao.getById(1));
        System.out.println("\nGetAllByCat");
        for (MetaTask task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore

    public void testGetAll() throws Exception {
        List<MetaTask> tasks = metaTaskDao.getAll();
        System.out.println("\nGetAll");
        for (MetaTask task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore
    public void testGetAllByTimeScale() throws Exception {
        List<MetaTask> tasks = metaTaskDao.getAllByTimeScale(timeScaleDao.getById(2));
        System.out.println("\nGetAllByTimeScale");
        for (MetaTask task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    public void testUpdate() throws Exception {
        MetaTask metaTask = metaTaskDao.getById(54);
        List<Task> tasks = metaTask.getSubTasks();
        tasks.add(taskDao.getById(16));
        metaTask.setSubTasks(tasks);
        metaTaskDao.update(metaTask);
    }
}