package planner.dao.impl;

import org.junit.Ignore;
import org.junit.Test;
import planner.descriptor.searchFilter.SearchCriteria;
import planner.model.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by �� on 2/9/2016.
 */
public class TaskDaoImplTest {
    TaskDaoImpl taskDao = TaskDaoImpl.getInstance();
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();
    UserDaoImpl userDao = UserDaoImpl.getInstance();
    TagDaoImpl tagDao = TagDaoImpl.getInstance();
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();
    MetaTaskDaoImpl metaTaskDao = MetaTaskDaoImpl.getInstance();


    @Test
    @Ignore
    public void testAdd() throws Exception {
        Task task = new Task();
        task.setTitle("test tags");
        task.setDescription("test tags");
        task.setPriority(5);

        //
        Date dueDate = new Date(666121312313123l);
        task.setDueDate(dueDate);

        Date startDate = new Date(343532552l);
        task.setStartDate(startDate);

        TimeScale timeScale = timeScaleDao.getById(3);
        task.setTimeScale(timeScale);

        Category category = categoryDao.getById(1);
        task.setCategory(category);

        List<Tag> tags = new ArrayList<Tag>();
        tags.add(tagDao.getById(7));
        tags.add(tagDao.getById(14));
        task.setTags(tags);

        task.setCompleteness(false);

        taskDao.add(task);
    }

    @Test
    @Ignore
    public void testDelete() throws Exception {
        Task task = taskDao.getById(3);
        taskDao.delete(task);
    }

    @Ignore

    @Test
    public void testGetById() throws Exception {
        Task task = taskDao.getById(3);
        System.out.println("GetByID");
        System.out.println(task.getId() + "   " + task.getTitle() + " " +
                task.getDescription() + "   " + task.getPriority() + "  " +
                task.getDueDate().toString() + "    " + task.getStartDate() +
                " " + task.isCompleteness());
    }

    @Test
    @Ignore
    public void testGetAllByTag() throws Exception {
        List<Task> tasks = taskDao.getAllByTag(tagDao.getById(1));
        System.out.println("\nGetAllByTag");
        for (Task task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore
    public void testGetAllByCategory() throws Exception {
        List<Task> tasks = taskDao.getAllByCategory(categoryDao.getById(1));
        System.out.println("\nGetAllByCat");
        for (Task task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore
    public void testGetAll() throws Exception {
        List<Task> tasks = taskDao.getAll();
        System.out.println("\nGetAll");
        for (Task task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    @Ignore
    public void testGetAllByTimeScale() throws Exception {
        List<Task> tasks = taskDao.getAllByTimeScale(timeScaleDao.getById(2));
        System.out.println("\nGetAllByTimeScale");
        for (Task task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Test
    public void testGetAllByMetaTask() throws Exception {
        List<Task> tasks = taskDao.getAllByMetaTask(metaTaskDao.getById(54));
        System.out.println("\nGetAllByMetatask");
        for (Task task : tasks)
            System.out.println(task.getId() + "   " + task.getTitle() +
                    " " + task.getDescription() + "   " + task.getPriority() +
                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
                    " " + task.isCompleteness());
    }

    @Ignore
    @Test
    public void testUpdate() throws Exception {
        Task task = taskDao.getById(50);
        task.setTitle("be a fat bastard");

        List<Tag> tags = new ArrayList<Tag>();
        tags.add(tagDao.getById(8));
        task.setTags(tags);

        taskDao.update(task);
    }

    @Test
    @Ignore
    public void testSearch() throws Exception {
//        SearchCriteria searchCriteria = new SearchCriteria();
//        searchCriteria.setTimeScale(timeScaleDao.getById(2));
//        searchCriteria.setCategory(categoryDao.getById(1));
//        Tag tag = tagDao.getById(1);
//        List<Tag> tags = new ArrayList<Tag>();
//        tags.add(tag);
//        //searchCriteria.setTags(tags);
//        List<Task> tasks = taskDao.searchAND(searchCriteria);
//        System.out.println("\nSearch");
//        for (Task task : tasks)
//            System.out.println(task.getId() + "   " + task.getTitle() +
//                    " " + task.getDescription() + "   " + task.getPriority() +
//                    "  " + task.getDueDate().toString() + "    " + task.getStartDate() +
//                    " " + task.isCompleteness());
    }
}