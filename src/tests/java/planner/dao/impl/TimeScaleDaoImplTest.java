package planner.dao.impl;

import org.junit.Ignore;
import org.junit.Test;
import planner.model.TimeScale;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertNotNull;

/**
 * Created by �� on 2/4/2016.
 */
public class TimeScaleDaoImplTest {
    TimeScaleDaoImpl timeScaleDao = TimeScaleDaoImpl.getInstance();

    @Test
    public void testAdd() throws Exception {
        TimeScale timeScale = new TimeScale();
        timeScale.setScale("fiveYears");
        timeScaleDao.add(timeScale);
    }

    @Test
    public void testGetById() throws Exception {
        TimeScale timeScale = timeScaleDao.getById(2);
        System.out.println("getById");
        System.out.println(timeScale.getId() + "     " + timeScale.getScale());
    }

    @Test
    public void testGetAll() throws Exception {
        List<TimeScale> timeScales = timeScaleDao.getAll();
        System.out.println("getAll");
        for (TimeScale timeScale : timeScales) {
            System.out.println(timeScale.getId() + "     " + timeScale.getScale());
        }

    }

    @Test
    public void testUpdate() throws Exception {
        TimeScale timeScale = timeScaleDao.getById(34);
        timeScale.setScale("manyYears");
        timeScaleDao.update(timeScale);
        System.out.println("update");
        timeScale = timeScaleDao.getById(34);
        System.out.println(timeScale.getId() + "     " + timeScale.getScale());
    }
}