package planner.dao.impl;

import org.junit.Ignore;
import org.junit.Test;
import planner.model.MetaTask;
import planner.model.Tag;
import planner.model.Task;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by �� on 2/7/2016.
 */
public class TagDaoImplTest {
    TagDaoImpl tagDao = TagDaoImpl.getInstance();
    TaskDaoImpl taskDao = TaskDaoImpl.getInstance();
    MetaTaskDaoImpl metaTaskDao = MetaTaskDaoImpl.getInstance();

    @Ignore
    @Test
    public void testAdd() throws Exception {
        Tag tag = new Tag();
        tag.setName("fun");
        tagDao.add(tag);
    }

    @Test
    public void testDelete() throws Exception {
        tagDao.delete(tagDao.getById(16));
    }

    @Test
    public void testGetById() throws Exception {
        Tag tag = tagDao.getById(12);
        System.out.println("\nget");
        System.out.println(tag.getId() + "     " + tag.getName());
    }

    @Test
    public void testGetAll() throws Exception {
        List<Tag> tags = tagDao.getAll();
        System.out.println("getAll\n");
        for (Tag tag : tags) {
            System.out.println(tag.getId() + "     " + tag.getName());
        }
    }

    @Ignore
    @Test
    public void testUpdate() throws Exception {
        Tag tag = tagDao.getById(1);
        tag.setName("funny");
        tagDao.update(tag);
    }


    @Ignore
    @Test
    public void testGetAllByTask() throws Exception {
        Task task = taskDao.getById(3);
        List<Tag> tags = tagDao.getAllByTask(task);
        System.out.println("getByTask\n");
        for (Tag tag : tags) {
            System.out.println(tag.getId() + "     " + tag.getName());
        }
    }

    @Test
    public void testGetAllByMeTaTask() throws Exception {
        MetaTask task = metaTaskDao.getById(7);
        List<Tag> tags = tagDao.getAllByMeTaTask(task);
        System.out.println("getByMeTaTask\n");
        for (Tag tag : tags) {
            System.out.println(tag.getId() + "     " + tag.getName());
        }
    }


}