package planner.dao.impl;

import org.junit.Ignore;
import org.junit.Test;
import planner.model.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by �� on 2/2/2016.
 */
public class UserDaoImplTest {
    UserDaoImpl userDao = UserDaoImpl.getInstance();

    @Test
    public void testAdd() throws Exception {
        User user = new User();
        user.setFirstName("Mike");
        user.setLastName("Portnoy");
        user.setEMail("m.portnoy@DT.com");
        user.setLogin("portnoy");
        user.setPassword("23");
        userDao.add(user);
    }

    @Ignore

    @Test
    public void testGetById() throws Exception {
        User user = new User();
        user.setFirstName("Mike");
        user.setLastName("Portnoy");
        user.setEMail("m.portnoy@DT.com");
        user.setLogin("portnoy");
        user.setPassword("23");
        User otherUser = userDao.getByLogin(user.getLogin());
        if (!otherUser.getLogin().equals(""))
            assertEquals("lala", user.getLogin(), otherUser.getLogin());
        System.out.println("\nget");
        System.out.println("login: " + user.getLogin() + " pass: " + user.getPassword());
    }

    @Test
    public void testGetAll() throws Exception {
        List<User> users = userDao.getAll();
        assertNotNull(users);
        System.out.println("\ngetAll");
        for (User user : users) {
            System.out.println("login: " + user.getLogin() + " pass: " + user.getPassword());
        }
    }

    @Test
    public void testDelete() throws Exception {
        userDao.delete(userDao.getByLogin("portnoy"));
    }

    @Test
    public void testUpdate() throws Exception {
        User user = userDao.getByLogin("portnoy");
        user.setFirstName("John");
        user.setEMail("j.portnoy@DT.com");
        user.setLogin("johny");
        userDao.update(user);
    }

    @Test
    public void testVerify() {
        String[] userInfo = new String[]{"login", "13"};
        boolean test = userDao.verifyUser(userInfo);
        int test2 = 9;
    }
}