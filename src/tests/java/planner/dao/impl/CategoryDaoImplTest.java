package planner.dao.impl;


import org.junit.Ignore;
import org.junit.Test;
import planner.model.Category;
import planner.model.Task;

import java.util.List;

/**
 * Created by �� on 2/5/2016.
 */
public class CategoryDaoImplTest {
    CategoryDaoImpl categoryDao = CategoryDaoImpl.getInstance();
    UserDaoImpl userDao = UserDaoImpl.getInstance();

    @Test
    public void testAdd() throws Exception {
        Category category = new Category();
        category.setName("dance");
        category.setDescription("write new music");
        categoryDao.add(category);
        System.out.println("Add\n");
    }


    @Test
    public void testGetById() throws Exception {
        Category category = categoryDao.getById(5);
        System.out.println("getById");
        System.out.println(category.getId() + "     " + category.getName() + "   " + category.getDescription() + "\n");
    }

    @Test
    public void testGetAll() throws Exception {
        List<Category> categories = categoryDao.getAll();
        System.out.println("getAll\n");
        for (Category category : categories) {
            System.out.println(category.getId() + "     " + category.getName() + "   " + category.getDescription());
        }
    }

    @Test
    @Ignore
    public void testDelete() throws Exception {
        categoryDao.delete(categoryDao.getById(5));
        System.out.println("delete 5 \n");
        for (Category category : categoryDao.getAll()) {
            System.out.println(category.getId() + "     " + category.getName() + "   " + category.getDescription());
        }
    }

    @Test
    public void testUpdate() throws Exception {
        Category category = new Category();
        category = categoryDao.getById(1);
        category.setName("workout");
        categoryDao.update(category);
        System.out.println("\nupdate 1");
        for (Category cat : categoryDao.getAll()) {
            System.out.println(cat.getId() + "     " + cat.getName() + "   " + cat.getDescription());
        }
    }

    @Test
    public void testGetAllByTask() throws Exception {
        TaskDaoImpl taskDao = new TaskDaoImpl();
        Task task = taskDao.getById(1);
        Category category = categoryDao.getByTask(task);
        System.out.println("getByTask\n");
            System.out.println(category.getId() + "     " + category.getName() + "   " + category.getDescription());

    }
}